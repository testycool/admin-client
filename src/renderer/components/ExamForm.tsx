import React from 'react';
import {
  Button,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
} from 'antd';
import { SaveFilled, StopOutlined } from '@ant-design/icons';
import { ExamStatusModel } from '../../shared/models/ExamModel';

export default function ExamForm(props: {
  initialState: any;
  onChange: (values: any) => void;
  onSubmit: (values: any) => void;
  onCancel: () => void;
  newExam: boolean;
}): JSX.Element {
  const { Option } = Select;
  const { initialState, onChange, onSubmit, onCancel, newExam } = props;

  const handleSubmit = (values: any) => {
    const startAt = new Date(values.startAt.get());
    const timeLimit =
      values.timeLimit.hours * 3600 + values.timeLimit.minutes * 60;

    onSubmit({
      password: values.password,
      title: values.title,
      startAt,
      timeLimit,
      status: values.status,
    });
  };

  return (
    <Form
      initialValues={initialState}
      onValuesChange={(_, allValues) => {
        return onChange(allValues);
      }}
      onFinish={handleSubmit}
      labelCol={{ span: 3 }}
      wrapperCol={{ span: 24 }}
      labelAlign="left"
      size={newExam ? 'large' : 'middle'}
      hideRequiredMark
    >
      <Form.Item name="title" label="Title" rules={[{ required: true }]}>
        <Input name="title" size="large" placeholder="Test Title" autoFocus />
      </Form.Item>
      <Form.Item
        label="Exam Password"
        name="password"
        rules={[{ required: true }]}
        style={{ width: '100%' }}
      >
        <Input placeholder="password" />
      </Form.Item>
      <Form.Item name="startAt" label="Start At" rules={[{ required: true }]}>
        <DatePicker style={{ width: '100%' }} showTime={{ format: 'HH:mm' }} />
      </Form.Item>
      <Form.Item label="Time Limit">
        <Input.Group compact>
          <Form.Item
            name={['timeLimit', 'hours']}
            rules={[{ required: true }]}
            style={{ margin: 0 }}
          >
            <InputNumber placeholder="HH" min={0} />
          </Form.Item>
          <Form.Item
            name={['timeLimit', 'minutes']}
            rules={[{ required: true }]}
            style={{ margin: 0 }}
          >
            <InputNumber placeholder="MM" min={0} max={59} />
          </Form.Item>
        </Input.Group>
      </Form.Item>
      {!newExam ? (
        <Form.Item
          name="status"
          label="status"
          rules={[{ required: true }]}
          initialValue={initialState.status || ExamStatusModel.WAITING}
        >
          <Select style={{ width: '100%' }}>
            <Option value={ExamStatusModel.WAITING}>Waiting</Option>
            <Option value={ExamStatusModel.STARTED}>Started</Option>
            <Option value={ExamStatusModel.DONE}>Done</Option>
          </Select>
        </Form.Item>
      ) : (
        ''
      )}
      <Form.Item>
        <Row justify="start">
          <Button
            htmlType="submit"
            type="primary"
            style={{ background: '#10C277', marginRight: '1em' }}
            size="middle"
          >
            <SaveFilled />
            {newExam ? 'CREATE EXAM' : 'UPDATE'}
          </Button>
          {!newExam ? (
            <Button type="primary" onClick={onCancel} size="middle" danger>
              <StopOutlined />
              CANCEL
            </Button>
          ) : (
            ''
          )}
        </Row>
      </Form.Item>
    </Form>
  );
}
