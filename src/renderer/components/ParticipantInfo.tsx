import React from 'react';
import { Button, Col, Row } from 'antd';
import { DeleteFilled, EditFilled } from '@ant-design/icons';
import { requestDeleteParticipant } from '../ipcRenderer/senders/participantSender';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import { LoadingType } from '../../shared/models/LoadingType';

export default function ParticipantInfo(props: {
  participant: { id: number; name: string; code: string };
  toggleEdit: () => void;
}): JSX.Element {
  const { participant, toggleEdit } = props;

  const loading = useSelector(
    (state: RootState) => state.ParticipantReducer.loading
  );

  const deleteParticipant = () => {
    requestDeleteParticipant(participant.id);
  };

  return (
    <Row>
      <Col span={2}>
        <p>{participant.id}</p>
      </Col>
      <Col span={12}>
        <p>{participant.name}</p>
      </Col>
      <Col span={6}>
        <p>{participant.code}</p>
      </Col>
      <Col span={4}>
        <Row justify="end">
          <Col>
            <Button
              type="primary"
              size="small"
              onClick={toggleEdit}
              disabled={loading !== LoadingType.Idle}
            >
              <EditFilled style={{ color: 'white' }} />
            </Button>{' '}
          </Col>
          <Col>
            <Button
              type="primary"
              danger
              size="small"
              style={{ marginLeft: '0.5rem' }}
              onClick={deleteParticipant}
              disabled={loading !== LoadingType.Idle}
            >
              <DeleteFilled style={{ color: 'white' }} />
            </Button>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
