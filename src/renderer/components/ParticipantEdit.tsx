import React from 'react';
import { Button, Col, Form, Input, Row } from 'antd';
import { SaveFilled, StopOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import { LoadingType } from '../../shared/models/LoadingType';

export default function ParticipantEdit(props: {
  participant: { id: number; name: string; code: string } | undefined;
  newParticipant: boolean;
  handleSubmit: (values: any) => void;
  toggleEdit: () => void;
}): JSX.Element {
  const { participant, newParticipant, handleSubmit, toggleEdit } = props;

  const loading = useSelector(
    (state: RootState) => state.ParticipantReducer.loading
  );

  const initialValue = () => {
    if (participant) {
      return {
        ...participant,
      };
    }
    return {
      id: undefined,
      name: undefined,
      code: undefined,
    };
  };

  return (
    <Row style={{ width: '100%' }}>
      <Form
        colon={false}
        initialValues={initialValue()}
        onFinish={handleSubmit}
        layout="inline"
        style={{ width: '100%' }}
      >
        {!newParticipant ? (
          <Col span={2}>
            <Form.Item name="id" rules={[{ required: true }]}>
              <Input placeholder="Participant ID" readOnly disabled />
            </Form.Item>
          </Col>
        ) : (
          ''
        )}
        <Col span={newParticipant ? 14 : 12}>
          <Form.Item name="name" rules={[{ required: true }]}>
            <Input placeholder="Participant Name" />
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item name="code" rules={[{ required: true }]}>
            <Input placeholder="Code" />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item style={{ margin: 0 }}>
            <Row justify="end">
              <Col>
                <Button
                  htmlType="submit"
                  type="primary"
                  size="small"
                  style={{ marginRight: '0.5rem' }}
                  disabled={loading !== LoadingType.Idle}
                >
                  <SaveFilled />
                </Button>
              </Col>
              <Col>
                <Button
                  onClick={toggleEdit}
                  type="primary"
                  size="small"
                  danger
                  disabled={loading !== LoadingType.Idle}
                >
                  <StopOutlined />
                </Button>
              </Col>
            </Row>
          </Form.Item>
        </Col>
      </Form>
    </Row>
  );
}
