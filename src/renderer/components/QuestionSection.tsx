import React, { useEffect } from 'react';
import { Button, Card, Col, List, Row, Typography } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import QuestionItem from './QuestionItem';
import { requestListQuestions } from '../ipcRenderer/senders/questionSender';
import { Link } from 'react-router-dom';

export default function QuestionSection(): JSX.Element {
  const { Title } = Typography;

  const examId = useSelector(
    (state: RootState) => state.ExamReducer.activeExam?.id
  );
  const questions = useSelector(
    (state: RootState) => state.QuestionReducer.questions
  );

  useEffect(() => {
    if (examId) {
      requestListQuestions(-1, 1, examId);
    }
  }, [examId]);

  return (
    <Card style={{ marginTop: '1rem' }}>
      <Row justify="space-between" align="middle">
        <Col>
          <Title style={{ color: '#175873' }}>Questions</Title>
        </Col>
        <Col>
          <Button type="primary" style={{ background: '#10C277', right: 0 }}>
            <Link to={`manage-question`}>
              <PlusOutlined style={{ color: 'white', fontSize: '1rem' }} />
              ADD NEW QUESTIONS
            </Link>
          </Button>
        </Col>
      </Row>
      <List
        dataSource={questions}
        renderItem={(question, index) => (
          <List.Item>
            <QuestionItem index={index + 1} question={question} />
          </List.Item>
        )}
      />
    </Card>
  );
}
