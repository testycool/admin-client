import React from 'react';
import { Button, Col, Form, Input, Row, Switch } from 'antd';
import { SaveFilled, StopOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import AttemptModel from '../../shared/models/AttemptModel';
import { LoadingType } from '../../shared/models/LoadingType';

export default function AttemptEdit(props: {
  attempt: AttemptModel;
  handleSubmit: (values: any) => void;
  toggleEdit: () => void;
}): JSX.Element {
  const { attempt, handleSubmit, toggleEdit } = props;

  const loading = useSelector(
    (state: RootState) => state.AttemptReducer.loading
  );

  const initialValue = () => {
    return {
      ...attempt,
    };
  };

  return (
    <Row style={{ width: '100%' }}>
      <Form
        colon={false}
        initialValues={initialValue()}
        onFinish={handleSubmit}
        layout="inline"
        style={{ width: '100%' }}
      >
        <Col span={3}>
          <Form.Item name="participantId" rules={[{ required: true }]}>
            <Input placeholder="participant ID" readOnly disabled />
          </Form.Item>
        </Col>
        <Col span={3}>
          <Form.Item name="finished" rules={[{ required: true }]}>
            <Switch defaultChecked={initialValue().finished} />
          </Form.Item>
        </Col>
        <Col span={2}>
          <Form.Item name="corrects" rules={[{ required: true }]}>
            <Input placeholder="Corrects" readOnly disabled />
          </Form.Item>
        </Col>
        <Col span={3}>
          <Form.Item name="wrongs" rules={[{ required: true }]}>
            <Input placeholder="Corrects" readOnly disabled />
          </Form.Item>
        </Col>
        <Col span={3}>
          <Form.Item name="unanswered" rules={[{ required: true }]}>
            <Input placeholder="Corrects" readOnly disabled />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item name="createdAt" rules={[{ required: true }]}>
            <Input placeholder="Created At" readOnly disabled />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item name="updatedAt" rules={[{ required: true }]}>
            <Input placeholder="Updated At" readOnly disabled />
          </Form.Item>
        </Col>
        <Col span={2}>
          <Form.Item style={{ margin: 0 }}>
            <Row justify="end">
              <Col>
                <Button
                  htmlType="submit"
                  type="primary"
                  size="small"
                  style={{ marginRight: '0.5rem' }}
                  disabled={loading !== LoadingType.Idle}
                >
                  <SaveFilled />
                </Button>
              </Col>
              <Col>
                <Button
                  onClick={toggleEdit}
                  type="primary"
                  size="small"
                  danger
                  disabled={loading !== LoadingType.Idle}
                >
                  <StopOutlined />
                </Button>
              </Col>
            </Row>
          </Form.Item>
        </Col>
      </Form>
    </Row>
  );
}
