import React, { useEffect, useState } from 'react';
import { Button, Card, Col, notification, Row, Space, Typography } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import ParticipantRow from './ParticipantRow';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../store';
import { requestListParticipants } from '../ipcRenderer/senders/participantSender';
import { setError } from '../../shared/actions/ParticipantAction';

export default function ParticipantsSection(props: {
  examId: number;
}): JSX.Element {
  const { Title } = Typography;

  const { examId } = props;

  const participantList = useSelector(
    (state: RootState) => state.ParticipantReducer.participants
  );
  const participantError = useSelector(
    (state: RootState) => state.ParticipantReducer.error
  );

  const dispatch = useDispatch<AppDispatch>();
  useEffect(() => {
    requestListParticipants(-1, 1, examId);
    if (participantError) {
      notification.error({
        message: 'ERROR',
        description: `${participantError.code}: ${participantError.details}`,
      });
      dispatch(setError({ err: undefined }));
    }
  }, [examId, participantError, dispatch]);

  const [newParticipant, setNewParticipant] = useState(false);

  return (
    <Card style={{ marginTop: '1rem' }}>
      <Row justify="space-between" align="middle">
        <Col>
          <Title style={{ color: '#175873' }}>Participants</Title>
        </Col>
        <Col>
          {!newParticipant ? (
            <Button
              type="primary"
              style={{ background: '#10C277', borderColor: '#10C277' }}
              onClick={() => setNewParticipant(true)}
            >
              <PlusOutlined style={{ fontSize: '1rem' }} />
            </Button>
          ) : (
            ''
          )}
        </Col>
      </Row>
      {newParticipant ? (
        <Space
          direction="vertical"
          style={{ width: '100%', marginBottom: '2em' }}
        >
          <Title level={5} style={{ fontWeight: 'bold', margin: 0 }}>
            New Participant
          </Title>
          <ParticipantRow
            newParticipant
            toggleNewParticipant={() => setNewParticipant(false)}
          />
        </Space>
      ) : (
        ''
      )}
      <Row>
        <Col span={2}>
          <Title level={5} style={{ fontWeight: 'bold' }}>
            ID
          </Title>
        </Col>
        <Col span={12}>
          <Title level={5} style={{ fontWeight: 'bold' }}>
            Participants
          </Title>
        </Col>
        <Col span={6}>
          <Title level={5} style={{ fontWeight: 'bold' }}>
            Code
          </Title>
        </Col>
        <Col span={4}>
          <Title level={5} style={{ fontWeight: 'bold', textAlign: 'end' }}>
            Action
          </Title>
        </Col>
      </Row>
      {participantList.map((participant) => (
        <ParticipantRow
          key={participant.id}
          participant={{
            id: participant.id,
            name: participant.name,
            code: participant.code,
          }}
          newParticipant={false}
        />
      ))}
    </Card>
  );
}
