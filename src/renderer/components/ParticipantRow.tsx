import React, { useState } from 'react';
import ParticipantEdit from './ParticipantEdit';
import ParticipantInfo from './ParticipantInfo';
import {
  requestCreateParticipant,
  requestUpdateParticipant,
} from '../ipcRenderer/senders/participantSender';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import { notification } from 'antd';

export default function ParticipantRow(props: {
  participant?: { id: number; name: string; code: string } | undefined;
  newParticipant: boolean;
  toggleNewParticipant?: () => void;
}): JSX.Element {
  const { newParticipant, participant, toggleNewParticipant } = props;

  const examId = useSelector(
    (state: RootState) => state.ExamReducer.activeExam?.id
  );

  const [editing, setEditing] = useState(newParticipant);
  const toggleEdit = () => {
    setEditing((prevState) => !prevState);
    if (toggleNewParticipant) {
      toggleNewParticipant();
    }
  };

  const participantList = useSelector(
    (state: RootState) => state.ParticipantReducer.participants
  );

  const checkDuplicateCode = (code: string, callback: () => void) => {
    const sameCode = participantList.find((p) => p.code === code);
    if (sameCode) {
      return notification.error({
        message: 'ERROR',
        description: `Participant with code ${code} already exist in current exam`,
      });
    } else {
      callback();
    }
  };

  const handleSubmit = (values: any) => {
    if (newParticipant) {
      if (values.name && values.code) {
        checkDuplicateCode(values.code, () => {
          requestCreateParticipant({
            ...values,
            examId,
          });
          toggleEdit();
        });
      }
      if (toggleNewParticipant) {
        toggleNewParticipant();
      }
    } else {
      if (
        values.name !== participant?.name ||
        values.code !== participant?.code
      ) {
        checkDuplicateCode(values.code, () => {
          requestUpdateParticipant({
            ...values,
            id: participant?.id,
            examId,
          });
          toggleEdit();
        });
      }
    }
  };

  return (
    <div>
      {editing || !participant ? (
        <ParticipantEdit
          participant={participant}
          toggleEdit={toggleEdit}
          handleSubmit={handleSubmit}
          newParticipant={newParticipant}
        />
      ) : (
        <ParticipantInfo participant={participant} toggleEdit={toggleEdit} />
      )}
    </div>
  );
}
