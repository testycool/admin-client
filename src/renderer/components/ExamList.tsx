import React, { useState } from 'react';
import { Button, Card, Col, Modal, Row, Space, Table } from 'antd';
import { ColumnType } from 'antd/lib/table';
import { DeleteFilled, EditFilled } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { ExamModel } from '../../shared/models/ExamModel';
import { SecondsToDuration } from '../../shared/libs/TimeParser';
import { StatusModelToString } from '../../shared/libs/StatusParser';
import {
  changeTab,
  createTab,
  removeTab,
} from '../../shared/actions/TabAction';
import { AppDispatch, rendererStore, RootState } from '../store';
import { requestDeleteExam } from '../ipcRenderer/senders/examSender';

export default function ExamList(props: {
  examList: ExamModel[];
}): JSX.Element {
  const { examList } = props;
  const tabList = useSelector((state: RootState) => state.TabReducer.tabs);

  const dispatch = useDispatch<AppDispatch>();
  const history = useHistory();
  const openTab = (exam?: ExamModel) => {
    if (exam) {
      const openExam = tabList.find(
        (tab) => tab.id === exam.id && !tab.newExamTab
      );
      if (openExam) {
        dispatch(changeTab({ id: openExam.id }));
      } else {
        dispatch(createTab({ newExam: false, exam }));
      }
    } else {
      dispatch(createTab({ newExam: true }));
    }
    const { activeTab } = rendererStore.getState().TabReducer;
    if (activeTab) {
      history.push(`/exam/${activeTab.id}/detail`);
    }
  };

  const [modalVisible, setModalVisible] = useState(false);
  const [selectedExam, setSelectedExam] = useState<ExamModel | undefined>(
    undefined
  );

  const openDeleteModal = (id: number) => {
    if (examList) {
      setSelectedExam(examList[id]);
    }
    setModalVisible(true);
  };

  const examDelete = (id: number) => {
    requestDeleteExam(id);
    const deletedExamTab = tabList.find((tab) => tab.id === id);
    if (deletedExamTab) {
      dispatch(removeTab({ id: deletedExamTab.id }));
    }
    setModalVisible(false);
  };

  const columns: ColumnType<any>[] = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Time Limit',
      dataIndex: 'timeLimit',
      key: 'timeLimit',
      render(_text: any, record: any) {
        return SecondsToDuration(record.timeLimit);
      },
    },
    {
      title: 'Start At',
      dataIndex: 'startAt',
      key: 'startAt',
      render(_text: any, record: any) {
        return moment(record.startAt).format('DD MMMM YYYY HH:mm');
      },
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render(_text: any, record: any) {
        return StatusModelToString(record.status);
      },
    },
    {
      title: 'Action',
      key: 'action',
      align: 'right',
      render(_text: any, record: any, index) {
        return (
          <Row justify="end" gutter={10}>
            <Col>
              <Button
                onClick={() => openTab(record)}
                type="primary"
                size="small"
              >
                <EditFilled />
              </Button>
            </Col>
            <Col>
              <Button
                onClick={() => openDeleteModal(index)}
                type="primary"
                size="small"
                danger
              >
                <DeleteFilled />
              </Button>
            </Col>
          </Row>
        );
      },
    },
  ];

  return (
    <Card style={{ width: '100%' }} bodyStyle={{ padding: 0 }}>
      <Table
        rowKey="id"
        columns={columns}
        pagination={{
          position: ['bottomCenter'],
        }}
        dataSource={examList}
        bordered
        footer={() => (
          <Button onClick={() => openTab()} style={{ width: '100%' }}>
            CREATE NEW EXAM
          </Button>
        )}
      />
      <Modal
        visible={modalVisible}
        title="Delete Exam"
        onOk={() => {
          if (selectedExam) {
            examDelete(selectedExam.id);
          }
        }}
        okText="DELETE"
        okType="danger"
        onCancel={() => setModalVisible(false)}
        cancelText="CANCEL"
      >
        <Space>
          <p style={{ textAlign: 'center' }}>
            {`Are you sure you want to delete ${selectedExam?.title}?`}
          </p>
        </Space>
      </Modal>
    </Card>
  );
}
