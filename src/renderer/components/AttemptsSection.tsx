import React, { useEffect } from 'react';
import { Card, Col, notification, Row, Typography } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../store';
import AttemptRow from './AttemptRow';
import attemptSender from '../ipcRenderer/senders/attemptSender';
import attemptAction from '../../shared/actions/AttemptAction';

export default function AttemptsSection(props: {
  examId: number;
}): JSX.Element {
  const { Title } = Typography;

  const { examId } = props;

  const attemptList = useSelector(
    (state: RootState) => state.AttemptReducer.attempts
  );
  const attemptError = useSelector(
    (state: RootState) => state.AttemptReducer.error
  );

  const dispatch = useDispatch<AppDispatch>();
  useEffect(() => {
    attemptSender.listAttempts({ filter: { examId }, size: -1, page: 1 });
    if (attemptError) {
      notification.error({
        message: 'ERROR',
        description: `${attemptError.code}: ${attemptError.details}`,
      });
      dispatch(attemptAction.setError(undefined));
    }
  }, [examId, attemptError, dispatch]);

  return (
    <Card style={{ marginTop: '1rem' }}>
      <Row justify="space-between" align="middle">
        <Col>
          <Title style={{ color: '#175873' }}>Attempts</Title>
        </Col>
      </Row>
      <Row>
        <Col span={3}>
          <Title level={5} style={{ fontWeight: 'bold' }}>
            Participant ID
          </Title>
        </Col>
        <Col span={3}>
          <Title level={5} style={{ fontWeight: 'bold' }}>
            Status
          </Title>
        </Col>
        <Col span={2}>
          <Title level={5} style={{ fontWeight: 'bold' }}>
            Corrects
          </Title>
        </Col>
        <Col span={3}>
          <Title level={5} style={{ fontWeight: 'bold' }}>
            Wrongs
          </Title>
        </Col>
        <Col span={3}>
          <Title level={5} style={{ fontWeight: 'bold' }}>
            Unanswered
          </Title>
        </Col>
        <Col span={4}>
          <Title level={5} style={{ fontWeight: 'bold' }}>
            Start at
          </Title>
        </Col>
        <Col span={4}>
          <Title level={5} style={{ fontWeight: 'bold' }}>
            Finished At
          </Title>
        </Col>
        <Col span={2}>
          <Title level={5} style={{ fontWeight: 'bold', textAlign: 'end' }}>
            Action
          </Title>
        </Col>
      </Row>
      {attemptList.map((attempt) => (
        <AttemptRow key={attempt.participantId} attempt={attempt} />
      ))}
    </Card>
  );
}
