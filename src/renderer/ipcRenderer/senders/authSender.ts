// eslint-disable-next-line import/prefer-default-export
export const sendAuthRequest = (address: string, passcode: string) => {
  window.auth.ipcRenderer.sendAuthRequest(address, passcode);
};
