import { rendererStore } from '../../store';
import {
  clearQuestions,
  createQuestion,
  deleteQuestion,
  getQuestion,
  listQuestions,
  setQuestionError,
  updateQuestion,
  updateQuestionType,
} from '../../../shared/actions/QuestionAction';
import { NumberToTypeModel } from '../../../shared/libs/QuestionTypeParser';
import {
  CreateQuestionResponse,
  GetQuestionResponse,
  Question,
  UpdateQuestionResponse,
} from '../../../main/services/protobuf/gen/testycool/question';
import { NumberToFormatModel } from '../../../shared/libs/TextFormatTypeParser';
import { requestCreateQuestion } from '../senders/questionSender';
import { createEmptyQuestion } from '../../../shared/libs/templates';
import { QuestionTypeModel } from '../../../shared/models/QuestionModel';
import { requestListChoices } from '../senders/choiceSender';
import { clearChoices } from '../../../shared/actions/ChoiceAction';

export default function questionListener() {
  window.question.ipcRenderer.on(
    'list-questions',
    (response: {
      size: number;
      page: number;
      questions: Question[];
      totalSize: number;
      examId: number;
    }) => {
      if (response.size === 0) {
        rendererStore.dispatch(clearQuestions());
        return requestCreateQuestion(createEmptyQuestion(response.examId));
      }

      rendererStore.dispatch(
        listQuestions.fulfilled({
          ...response,
          questions: response.questions.map((question) => ({
            ...question,
            type: NumberToTypeModel(question.type),
            format: NumberToFormatModel(question.format),
          })),
        })
      );
    }
  );
  window.question.ipcRenderer.on(
    'get-question',
    (response: GetQuestionResponse) => {
      if (response.question) {
        rendererStore.dispatch(
          getQuestion.fulfilled({
            question: {
              ...response.question,
              type: NumberToTypeModel(response.question.type),
              format: NumberToFormatModel(response.question.format),
            },
          })
        );
        if (NumberToTypeModel(response.question.type) == QuestionTypeModel.MC) {
          requestListChoices(-1, 1, response.question.id);
        } else {
          rendererStore.dispatch(clearChoices());
        }
      }
    }
  );
  window.question.ipcRenderer.on(
    'create-question',
    (response: CreateQuestionResponse) => {
      if (response.question) {
        rendererStore.dispatch(
          createQuestion.fulfilled({
            question: {
              ...response.question,
              type: NumberToTypeModel(response.question.type),
              format: NumberToFormatModel(response.question.format),
            },
          })
        );
        if (NumberToTypeModel(response.question.type) == QuestionTypeModel.MC) {
          requestListChoices(-1, 1, response.question.id);
        } else {
          rendererStore.dispatch(clearChoices());
        }
      }
    }
  );
  window.question.ipcRenderer.on(
    'update-question',
    (response: UpdateQuestionResponse) => {
      if (response.question) {
        rendererStore.dispatch(
          updateQuestion.fulfilled({
            question: {
              ...response.question,
              type: NumberToTypeModel(response.question.type),
              format: NumberToFormatModel(response.question.format),
            },
          })
        );
        if (NumberToTypeModel(response.question.type) == QuestionTypeModel.MC) {
          requestListChoices(-1, 1, response.question.id);
        } else {
          rendererStore.dispatch(clearChoices());
        }
      }
    }
  );
  window.question.ipcRenderer.on(
    'update-question-type',
    (response: { question: Question; oldId: number }) => {
      if (response.question) {
        rendererStore.dispatch(
          updateQuestionType.fulfilled({
            question: {
              ...response.question,
              type: NumberToTypeModel(response.question.type),
              format: NumberToFormatModel(response.question.format),
            },
            oldId: response.oldId,
          })
        );
        if (NumberToTypeModel(response.question.type) == QuestionTypeModel.MC) {
          requestListChoices(-1, 1, response.question.id);
        } else {
          rendererStore.dispatch(clearChoices());
        }
      }
    }
  );
  window.question.ipcRenderer.on(
    'delete-question',
    (response: { id: number }) => {
      console.log(response);
      rendererStore.dispatch(deleteQuestion.fulfilled({ id: response.id }));
    }
  );
  window.question.ipcRenderer.on(
    'question-error',
    (args: { code: number; details: string }) => {
      rendererStore.dispatch(setQuestionError({ err: args }));
    }
  );
}
