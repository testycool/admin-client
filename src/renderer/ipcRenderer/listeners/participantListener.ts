import { rendererStore } from '../../store';
import {
  CreateParticipantResponse,
  Participant,
} from '../../../main/services/protobuf/gen/testycool/participant';
import {
  createParticipant,
  deleteParticipant,
  listParticipants,
  updateParticipant,
  setError,
} from '../../../shared/actions/ParticipantAction';

export default function participantListener() {
  window.participant.ipcRenderer.on(
    'list-participants',
    (response: {
      size: number;
      page: number;
      participants: Participant[];
      totalSize: number;
    }) => {
      rendererStore.dispatch(
        listParticipants.fulfilled({
          ...response,
        })
      );
    }
  );
  window.participant.ipcRenderer.on(
    'create-participant',
    (response: CreateParticipantResponse) => {
      if (response.participant) {
        rendererStore.dispatch(
          createParticipant.fulfilled({
            participant: {
              ...response.participant,
            },
          })
        );
      }
    }
  );
  window.participant.ipcRenderer.on(
    'update-participant',
    (response: CreateParticipantResponse) => {
      if (response.participant) {
        rendererStore.dispatch(
          updateParticipant.fulfilled({
            participant: {
              ...response.participant,
            },
          })
        );
      }
    }
  );
  window.participant.ipcRenderer.on(
    'delete-participant',
    (response: { id: number }) => {
      rendererStore.dispatch(deleteParticipant.fulfilled({ id: response.id }));
    }
  );
  window.participant.ipcRenderer.on(
    'participant-error',
    (response: { code: number; details: string }) => {
      console.log('error');
      rendererStore.dispatch(setError({ err: response }));
    }
  );
}
