import { rendererStore } from '../../store';
import { setAuthStatus, setError } from '../../../shared/actions/AuthAction';
import { AuthToken } from '../../../main/services/protobuf/gen/testycool/auth';

export default function authListener() {
  window.auth.ipcRenderer.on('auth-request', (args: AuthToken) => {
    if (args) {
      rendererStore.dispatch(setAuthStatus({ authenticated: true }));
      rendererStore.dispatch(setError({ err: undefined }));
    }
  });
  window.auth.ipcRenderer.on(
    'auth-error',
    (args: { code: number; detail: string }) => {
      rendererStore.dispatch(setError({ err: args }));
    }
  );
}
