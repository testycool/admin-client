import React, { useEffect } from 'react';
import { Card, Col, notification, Row } from 'antd';
import QuestionList from '../components/QuestionList';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../store';
import {
  requestGetQuestion,
  requestListQuestions,
} from '../ipcRenderer/senders/questionSender';
import { setError } from '../../shared/actions/ExamAction';
import Loading from '../components/Loading';
import QuestionForm from '../components/QuestionForm';
import {
  LoadingType,
  LoadingTypeToString,
} from '../../shared/models/LoadingType';

export default function ExamManageQuestion(): JSX.Element {
  const questions = useSelector(
    (state: RootState) => state.QuestionReducer.questions
  );
  const activeQuestionIdx = useSelector(
    (state: RootState) => state.QuestionReducer.activeQuestionIdx
  );
  const activeQuestion = useSelector(
    (state: RootState) => state.QuestionReducer.activeQuestion
  );
  const choices = useSelector(
    (state: RootState) => state.ChoiceReducer.choices
  );
  const examId = useSelector(
    (state: RootState) => state.ExamReducer.activeExam?.id
  );
  const questionLoading = useSelector(
    (state: RootState) => state.QuestionReducer.loading
  );
  const questionError = useSelector(
    (state: RootState) => state.QuestionReducer.error
  );

  const dispatch = useDispatch<AppDispatch>();
  useEffect(() => {
    if (questionError) {
      notification.error({
        message: 'ERROR',
        description: `${questionError.code}: ${questionError.details}`,
      });
      dispatch(setError({ err: undefined }));
    }
  }, [questionError, dispatch]);

  useEffect(() => {
    if (examId) {
      requestListQuestions(-1, 1, examId);
    }
  }, [examId]);

  useEffect(() => {
    if (questions.length > 0) {
      if (activeQuestion?.id !== questions[activeQuestionIdx].id) {
        requestGetQuestion(questions[activeQuestionIdx].id);
      }
    }
  }, [questions, activeQuestionIdx, activeQuestion, dispatch]);

  return (
    <Row align="top" justify="start" gutter={10} wrap={false}>
      <Col flex="21em">
        <Card>
          {questionLoading === LoadingType.GetList || questions.length === 0 ? (
            <Loading
              context={LoadingTypeToString(questionLoading, 'question')}
            />
          ) : (
            <QuestionList
              questions={questions}
              activeIndex={activeQuestionIdx}
            />
          )}
        </Card>
      </Col>
      <Col flex="auto">
        <Card>
          {questionLoading === LoadingType.GetEntry || !activeQuestion ? (
            <Loading
              context={LoadingTypeToString(questionLoading, 'question')}
            />
          ) : (
            <QuestionForm question={activeQuestion} choices={choices} />
          )}
        </Card>
      </Col>
    </Row>
  );
}
