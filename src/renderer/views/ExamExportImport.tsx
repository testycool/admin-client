import React from 'react';
import { Button, Card, Col, Row, Typography, Upload } from 'antd';
import { DownloadOutlined, FileTextFilled } from '@ant-design/icons';

export default function ExamExportImport(): JSX.Element {
  const { Title } = Typography;
  const { Dragger } = Upload;

  return (
    <Col>
      <div style={{ marginBottom: '3em' }}>
        <Title level={2}>Export</Title>
        <Row justify="space-between">
          <Button type="primary" size="large">
            <DownloadOutlined /> DOWNLOAD ANSWER RECAP
          </Button>
          <Button type="primary" size="large">
            <DownloadOutlined /> DOWNLOAD PARTICIPANT LIST
          </Button>
          <Button type="primary" size="large">
            <DownloadOutlined /> DOWNLOAD QUESTION LIST
          </Button>
        </Row>
      </div>
      <div>
        <Title level={2}>Import</Title>
        <Row gutter={10}>
          <Col flex={2}>
            <Card>
              <Title level={4}>Import Participant List</Title>
              <Dragger name="participant-list-file">
                <p className="ant-upload-drag-icon">
                  <FileTextFilled />
                </p>
                <p className="ant-upload-text">
                  Click or drag .csv file to this area to upload
                </p>
              </Dragger>
            </Card>
          </Col>
          <Col flex={2}>
            <Card>
              <Title level={4}>Import Question List</Title>
              <Dragger name="question-list-file">
                <p className="ant-upload-drag-icon">
                  <FileTextFilled />
                </p>
                <p className="ant-upload-text">
                  Click or drag .csv file to this area to upload
                </p>
              </Dragger>
            </Card>
          </Col>
        </Row>
      </div>
    </Col>
  );
}
