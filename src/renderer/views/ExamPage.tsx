import React from 'react';
import 'antd/dist/antd.css';
import { Layout } from 'antd';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import SideBar from '../components/SideBar';
import ExamDetail from './ExamDetail';
import ExamManageQuestion from './ExamManageQuestion';
import ExamExportImport from './ExamExportImport';

export default function ExamPage(): JSX.Element {
  const { path } = useRouteMatch();
  return (
    <Layout style={{ minHeight: '96vh' }}>
      <SideBar />
      <Layout
        style={{
          marginLeft: '13rem',
          width: '100%',
          padding: 48,
        }}
      >
        <Switch>
          <Route path={`${path}/detail`} component={ExamDetail} />
          <Route
            path={`${path}/manage-question`}
            component={ExamManageQuestion}
          />
          <Route path={`${path}/export-import`} component={ExamExportImport} />
        </Switch>
      </Layout>
    </Layout>
  );
}
