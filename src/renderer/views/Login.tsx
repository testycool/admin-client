import React, { useEffect } from 'react';
import {
  Button,
  Card,
  Form,
  Input,
  Layout,
  notification,
  Row,
  Space,
  Typography,
} from 'antd';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { sendAuthRequest } from '../ipcRenderer/senders/authSender';
import { AppDispatch, RootState } from '../store';
import { setError } from '../../shared/actions/AuthAction';

export default function Login(): JSX.Element {
  const { Content } = Layout;
  const { Title } = Typography;

  const authStatus = useSelector(
    (state: RootState) => state.AuthReducer.authStatus
  );
  const authError = useSelector((state: RootState) => state.AuthReducer.error);

  const loginRequest = (values: any) => {
    sendAuthRequest(values.address, values.passcode);
  };

  const history = useHistory();
  const dispatch = useDispatch<AppDispatch>();
  useEffect(() => {
    if (authStatus) {
      history.push('/dashboard');
    }
    if (authError) {
      notification.error({
        message: 'ERROR',
        description: `${authError.code}: ${authError.details}`,
      });
      dispatch(setError({ err: undefined }));
    }
  }, [authError, authStatus, history, dispatch]);

  return (
    <Layout style={{ height: '100vh' }}>
      <Content>
        <Row align="middle" justify="center" style={{ height: '100%' }}>
          <Space align="center" direction="vertical" style={{ width: '100%' }}>
            <Title>Welcome To Testy</Title>
            <Card style={{ width: '35vh' }}>
              <Space
                align="center"
                direction="vertical"
                style={{ width: '100%' }}
              >
                <Title level={3}>Input Credentials</Title>
                <Form onFinish={loginRequest}>
                  <Title level={4} style={{ textAlign: 'center' }}>
                    Server Address
                  </Title>
                  <Form.Item name="address" rules={[{ required: true }]}>
                    <Input placeholder="IP Address/Domain" autoFocus />
                  </Form.Item>
                  <Title level={4} style={{ textAlign: 'center' }}>
                    Passcode
                  </Title>
                  <Form.Item name="passcode" rules={[{ required: true }]}>
                    <Input.Password visibilityToggle={false} />
                  </Form.Item>
                  <Form.Item>
                    <Space
                      align="center"
                      direction="vertical"
                      style={{ width: '100%' }}
                    >
                      <Button htmlType="submit" type="primary">
                        LOGIN
                      </Button>
                    </Space>
                  </Form.Item>
                </Form>
              </Space>
            </Card>
          </Space>
        </Row>
      </Content>
    </Layout>
  );
}
