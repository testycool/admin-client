import React from 'react';
import 'antd/dist/antd.css';
import { Card, Row } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { AppDispatch, RootState } from '../store';
import { saveNewTab } from '../../shared/actions/TabAction';
import ExamForm from '../components/ExamForm';
import { requestCreateExam } from '../ipcRenderer/senders/examSender';

export default function ExamCreate(): JSX.Element {
  const activeTab = useSelector(
    (state: RootState) => state.TabReducer.activeTab
  );

  const initialState = () => {
    if (activeTab?.newExam) {
      return {
        ...activeTab.newExam,
        startAt: moment(activeTab.newExam.startAt),
        timeLimit: {
          hours: Math.floor(activeTab.newExam.timeLimit / 3600),
          minutes: (activeTab.newExam.timeLimit / 60) % 60,
        },
        status: undefined,
      };
    }
    return {
      title: undefined,
      pin: undefined,
      startAt: undefined,
      timeLimit: undefined,
      status: undefined,
    };
  };

  const dispatch = useDispatch<AppDispatch>();
  const handleFormChange = (newValues: {
    title: string;
    password: string;
    startAt?: any;
    timeLimit: { hours: number; minutes: number };
  }) => {
    const startAt = newValues.startAt?.get().toISOString();
    const timeLimit =
      newValues.timeLimit.hours * 3600 + newValues.timeLimit.minutes * 60;
    dispatch(
      saveNewTab({
        newExam: {
          password: newValues.password,
          title: newValues.title,
          startAt,
          timeLimit,
        },
      })
    );
  };

  const handleSubmit = (values: any) => {
    requestCreateExam({
      password: values.password,
      title: values.title,
      startAt: values.startAt,
      timeLimit: values.timeLimit,
    });
  };

  return (
    <Row
      align="middle"
      justify="center"
      style={{ height: '100%', padding: 48 }}
    >
      <Card style={{ width: '100%' }}>
        <ExamForm
          newExam
          onChange={handleFormChange}
          onSubmit={handleSubmit}
          initialState={initialState()}
          onCancel={() => {}}
        />
      </Card>
    </Row>
  );
}
