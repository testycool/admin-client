import React from 'react';
import ParticipantsSection from '../components/ParticipantsSection';
import AttemptsSection from '../components/AttemptsSection';
import QuestionSection from '../components/QuestionSection';
import ExamInfoSection from '../components/ExamInfoSection';
import { useSelector } from 'react-redux';
import { RootState } from '../store';

export default function ExamDetail(): JSX.Element {
  const exam = useSelector((state: RootState) => state.ExamReducer.activeExam);

  return (
    <div className="site-layout-background">
      {exam ? (
        <div>
          <ExamInfoSection />
          <ParticipantsSection examId={exam?.id} />
          <AttemptsSection examId={exam?.id} />
          <QuestionSection />
        </div>
      ) : (
        ''
      )}
    </div>
  );
}
