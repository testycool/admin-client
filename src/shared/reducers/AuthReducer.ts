import { createReducer } from '@reduxjs/toolkit';
import {
  setAccessToken,
  setAddress,
  setAuthStatus,
  setError,
} from '../actions/AuthAction';

export interface AuthState {
  address?: string;
  accessToken?: string;
  authStatus?: boolean;
  error?: any;
}

const initialState: AuthState = {
  address: undefined,
};

const AuthReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(setAddress, (state, action) => ({
      ...state,
      address: action.payload.address,
    }))
    .addCase(setAccessToken, (state, action) => ({
      ...state,
      accessToken: action.payload.accessToken,
    }))
    .addCase(setAuthStatus, (state, action) => ({
      ...state,
      authStatus: action.payload.authenticated,
    }))
    .addCase(setError, (state, action) => ({
      ...state,
      error: action.payload.err,
    }))
);

export default AuthReducer;
