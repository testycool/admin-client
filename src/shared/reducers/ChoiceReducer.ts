import { createReducer } from '@reduxjs/toolkit';
import { setError } from '../actions/AuthAction';
import {
  clearChoices,
  createChoice,
  deleteChoice,
  listChoices,
  updateChoice,
} from '../actions/ChoiceAction';
import ChoiceModel from '../models/ChoiceModel';
import { LoadingType } from '../models/LoadingType';

export interface ChoiceState {
  size: number;
  page: number;
  totalSize?: number;
  choices: ChoiceModel[];
  loading: LoadingType;
  error?: any;
}

const initialState: ChoiceState = {
  size: -1,
  page: 1,
  totalSize: undefined,
  choices: [],
  loading: LoadingType.Idle,
  error: undefined,
};

const ChoiceReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(listChoices.pending, (state) => ({
      ...state,
      loading: LoadingType.GetList,
    }))
    .addCase(listChoices.fulfilled, (state, action) => {
      return {
        ...state,
        size: action.payload.size,
        page: action.payload.page,
        choices: action.payload.choices,
        totalSize: action.payload.totalSize,
        loading: LoadingType.Idle,
      };
    })
    .addCase(listChoices.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(createChoice.pending, (state) => ({
      ...state,
      loading: LoadingType.Create,
    }))
    .addCase(createChoice.fulfilled, (state, action) => {
      state.choices.push(action.payload.choice);
      state.loading = LoadingType.Idle;
    })
    .addCase(createChoice.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(updateChoice.pending, (state) => ({
      ...state,
      loading: LoadingType.Update,
    }))
    .addCase(updateChoice.fulfilled, (state, action) => {
      const updatedChoiceIndex = state.choices.findIndex(
        (choice) => choice.id === action.payload.choice.id
      );
      state.choices[updatedChoiceIndex] = action.payload.choice;
      state.loading = LoadingType.Idle;
    })
    .addCase(updateChoice.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(deleteChoice.pending, (state) => ({
      ...state,
      loading: LoadingType.Delete,
    }))
    .addCase(deleteChoice.fulfilled, (state, action) => {
      const deletedExamIndex = state.choices.findIndex(
        (choice) => choice.id === action.payload.id
      );

      state.choices.splice(deletedExamIndex, 1);
      state.loading = LoadingType.Idle;
    })
    .addCase(deleteChoice.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(clearChoices, (state) => ({
      ...state,
      choices: [],
      loading: LoadingType.Idle,
    }))
    .addCase(setError, (state, action) => ({
      ...state,
      error: action.payload.err,
      loading: LoadingType.Idle,
    }))
);

export default ChoiceReducer;
