import { createReducer } from '@reduxjs/toolkit';
import { ExamModel } from '../models/ExamModel';
import {
  createExam,
  deleteExam,
  getExam,
  listExams,
  removeActiveExam,
  updateExam,
} from '../actions/ExamAction';
import { setError } from '../actions/AuthAction';
import { LoadingType } from '../models/LoadingType';

export interface ExamState {
  size: number;
  page: number;
  totalSize?: number;
  exams: ExamModel[];
  activeExam?: ExamModel;
  loading: LoadingType;
  error?: any;
}

const initialState: ExamState = {
  size: 10,
  page: 1,
  totalSize: undefined,
  exams: [],
  activeExam: undefined,
  loading: LoadingType.Idle,
};

const ExamReducer = createReducer(initialState, (builder) =>
  builder
    .addCase(listExams.pending, (state) => ({
      ...state,
      loading: LoadingType.GetList,
    }))
    .addCase(listExams.fulfilled, (state, action) => {
      return {
        ...state,
        size: action.payload.size,
        page: action.payload.page,
        exams: action.payload.exams,
        totalSize: action.payload.totalSize,
        loading: LoadingType.Idle,
      };
    })
    .addCase(listExams.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(getExam.pending, (state) => ({
      ...state,
      loading: LoadingType.GetEntry,
    }))
    .addCase(getExam.fulfilled, (state, action) => {
      return {
        ...state,
        activeExam: action.payload.exam,
        loading: LoadingType.Idle,
      };
    })
    .addCase(getExam.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(createExam.pending, (state) => ({
      ...state,
      loading: LoadingType.Create,
    }))
    .addCase(createExam.fulfilled, (state, action) => {
      return {
        ...state,
        activeExam: action.payload.exam,
        loading: LoadingType.Idle,
      };
    })
    .addCase(createExam.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(updateExam.pending, (state) => ({
      ...state,
      loading: LoadingType.Update,
    }))
    .addCase(updateExam.fulfilled, (state, action) => {
      return {
        ...state,
        activeExam: action.payload.exam,
        loading: LoadingType.Idle,
      };
    })
    .addCase(updateExam.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(deleteExam.pending, (state) => ({
      ...state,
      loading: LoadingType.Delete,
    }))
    .addCase(deleteExam.fulfilled, (state, action) => {
      const deletedExamIndex = state.exams.findIndex(
        (exam) => exam.id === action.payload.id
      );

      state.exams.splice(deletedExamIndex, 1);
      state.loading = LoadingType.Idle;
    })
    .addCase(deleteExam.rejected, (state, action) => {
      return {
        ...state,
        error: action.payload.err,
        loading: LoadingType.Idle,
      };
    })
    .addCase(setError, (state, action) => ({
      ...state,
      error: action.payload.err,
      loading: LoadingType.Idle,
    }))
    .addCase(removeActiveExam, (state) => {
      return {
        ...state,
        activeExam: undefined,
        loading: LoadingType.Idle,
      };
    })
);

export default ExamReducer;
