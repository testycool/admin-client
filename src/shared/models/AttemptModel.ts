export default interface AttemptModel {
  id: number;
  participantId: number;
  examId: number;
  finished: boolean;
  corrects: number;
  wrongs: number;
  unanswered: number;
  createdAt: Date | string | undefined;
  updatedAt: Date | string | undefined;
}
