import { TextFormatModel } from './QuestionModel';

export default interface ChoiceModel {
  id: number;
  questionId: number;
  isCorrect: boolean;
  format: TextFormatModel;
  content: string;
}
