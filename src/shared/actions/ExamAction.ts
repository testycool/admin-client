import { createAction } from '@reduxjs/toolkit';
import { ExamModel } from '../models/ExamModel';

export const listExams = {
  pending: createAction('[Exam] List all exams/pending'),
  fulfilled: createAction<{
    size: number;
    page: number;
    exams: ExamModel[];
    totalSize: number;
  }>('[Exam] List all exams/fulfilled'),
  rejected: createAction<{ err: any }>('[Exam] List all exams/rejected'),
};

export const getExam = {
  pending: createAction('[Exam] Get one exam/pending'),
  fulfilled: createAction<{ exam: ExamModel }>('[Exam] Get one exam/fulfilled'),
  rejected: createAction<{ err: any }>('[Exam] Get one exam/rejected'),
};

export const createExam = {
  pending: createAction('[Exam] Create new exam/pending'),
  fulfilled: createAction<{ exam: ExamModel }>(
    '[Exam] Create new exam/fulfilled'
  ),
  rejected: createAction<{ err: any }>('[Exam] Create new exam/rejected'),
};

export const updateExam = {
  pending: createAction('[Exam] Update one exam/pending'),
  fulfilled: createAction<{ exam: ExamModel }>(
    '[Exam] Update one exam/fulfilled'
  ),
  rejected: createAction<{ err: any }>('[Exam] Update one exam/rejected'),
};

export const deleteExam = {
  pending: createAction('[Exam] Delete one exam/pending'),
  fulfilled: createAction<{ id: number }>('[Exam] Delete one exam/fulfilled'),
  rejected: createAction<{ err: any }>('[Exam] Delete one exam/rejected'),
};

export const setError = createAction<{ err: any }>(
  '[Auth] set authentication error'
);

export const removeActiveExam = createAction('[Exam] remove active exam');
