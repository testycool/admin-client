import { createAction } from '@reduxjs/toolkit';
import { ListAttemptsResponseDto } from '../dto/ListAttemptsResponseDto';
import ErrorResponseDto from '../dto/ErrorResponseDto';
import { GetAttemptResponseDto } from '../dto/GetAttemptResponseDto';
import { UpdateAttemptResponseDto } from '../dto/UpdateAttemptResponseDto';

const attemptAction = {
  listAttempts: {
    pending: createAction('[Attempt] List Attempts/pending'),
    fulfilled: createAction<ListAttemptsResponseDto>(
      '[Attempt] List Attempts/fulfilled'
    ),
    rejected: createAction<ErrorResponseDto>(
      '[Attempt] List Attempts/rejected'
    ),
  },
  getAttempt: {
    pending: createAction('[Attempt] Get Attempt/pending'),
    fulfilled: createAction<GetAttemptResponseDto>(
      '[Attempt] Get Attempt/fulfilled'
    ),
    rejected: createAction<ErrorResponseDto>('[Attempt] Get Attempt/rejected'),
  },
  updateAttempt: {
    pending: createAction('[Attempt] Update Attempt/pending'),
    fulfilled: createAction<UpdateAttemptResponseDto>(
      '[Attempt] Update Attempt/fulfilled'
    ),
    rejected: createAction<ErrorResponseDto>(
      '[Attempt] Update Attempt/rejected'
    ),
  },
  setError: createAction<ErrorResponseDto | undefined>(
    '[Attempt] set exam error'
  ),
  resetState: createAction('[Attempt] reset state'),
};

export default attemptAction;
