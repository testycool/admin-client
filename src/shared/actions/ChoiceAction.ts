import { createAction } from '@reduxjs/toolkit';
import ChoiceModel from '../models/ChoiceModel';

export const listChoices = {
  pending: createAction('[Choice] List all choices/pending'),
  fulfilled: createAction<{
    size: number;
    page: number;
    choices: ChoiceModel[];
    totalSize: number;
  }>('[Choice] List all choices/fulfilled'),
  rejected: createAction<{ err: any }>('[Choice] List all choices/rejected'),
};

export const createChoice = {
  pending: createAction('[Choice] Create new choice/pending'),
  fulfilled: createAction<{ choice: ChoiceModel }>(
    '[Exam] Create new choice/fulfilled'
  ),
  rejected: createAction<{ err: any }>('[Choice] Create new choice/rejected'),
};

export const updateChoice = {
  pending: createAction('[Choice] Update one choice/pending'),
  fulfilled: createAction<{ choice: ChoiceModel }>(
    '[Exam] Update one choice/fulfilled'
  ),
  rejected: createAction<{ err: any }>('[Choice] Update one choice/rejected'),
};

export const deleteChoice = {
  pending: createAction('[Choice] Delete one choice/pending'),
  fulfilled: createAction<{ id: number }>(
    '[Choice] Delete one choice/fulfilled'
  ),
  rejected: createAction<{ err: any }>('[Choice] Delete one choice/rejected'),
};

export const clearChoices = createAction('[Choice] Clear choices variable');

export const setError = createAction<{ err: any }>('[Choice] set choice error');
