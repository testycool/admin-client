import { createAction } from '@reduxjs/toolkit';
import { ExamModel } from '../models/ExamModel';

export const listTabs = createAction('[Tab] list all open tabs');
export const createTab = createAction<{ newExam: boolean; exam?: ExamModel }>(
  '[Tab] create and open new tab'
);
export const changeTab = createAction<{ id: number }>('[Tab] change open tab');
export const removeTab = createAction<{ id: number }>('[Tab] remove tab');
export const saveNewTab = createAction<{
  newExam: {
    title: string;
    password: string;
    startAt: string;
    timeLimit: number;
  };
}>('[Tab] save new exam data');
export const updateTab =
  createAction<{ id: number; title: string }>('[Tab] update tab');
