import { configureStore } from '@reduxjs/toolkit';
import {
  AttemptReducer,
  AuthReducer,
  ChoiceReducer,
  ExamReducer,
  ParticipantReducer,
  QuestionReducer,
  TabReducer,
} from '../reducers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const configureElectronStore = (_scope = 'main') => {
  return configureStore({
    reducer: {
      AuthReducer,
      ExamReducer,
      ParticipantReducer,
      QuestionReducer,
      ChoiceReducer,
      AttemptReducer,
      TabReducer,
    },
  });
};

export default configureElectronStore;
