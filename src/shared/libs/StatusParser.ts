import { ExamStatusModel } from '../models/ExamModel';

export const StatusModelToString = (status?: ExamStatusModel) => {
  if (status) {
    switch (status) {
      case ExamStatusModel.WAITING:
        return 'Waiting';
      case ExamStatusModel.STARTED:
        return 'Started';
      case ExamStatusModel.DONE:
        return 'Done';
      default:
        return 'Unrecognized';
    }
  }
  return 'Unrecognized';
};

export const StatusNumberToStatusModel = (status: number) => {
  switch (status) {
    case 1:
      return ExamStatusModel.WAITING;
    case 2:
      return ExamStatusModel.STARTED;
    case 3:
      return ExamStatusModel.DONE;
    default:
      return ExamStatusModel.UNRECOGNIZED;
  }
};
