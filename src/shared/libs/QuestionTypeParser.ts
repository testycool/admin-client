import { QuestionTypeModel } from '../models/QuestionModel';

export const TypeModelToString = (type?: QuestionTypeModel) => {
  if (type) {
    switch (type) {
      case QuestionTypeModel.MC:
        return 'Multiple choice';
      case QuestionTypeModel.ES:
        return 'Essay';
      default:
        return 'Unrecognized';
    }
  }
  return 'Unrecognized';
};

export const TypeModelToNumber = (type: QuestionTypeModel) => {
  switch (type) {
    case QuestionTypeModel.MC:
      return 1;
    case QuestionTypeModel.ES:
      return 2;
    default:
      return 0;
  }
};

export const NumberToTypeModel = (type: number) => {
  switch (type) {
    case 1:
      return QuestionTypeModel.MC;
    case 2:
      return QuestionTypeModel.ES;
    default:
      return QuestionTypeModel.UNRECOGNIZED;
  }
};
