import { CreateQuestionRequest } from '../../main/services/protobuf/gen/testycool/question';
import { TypeModelToNumber } from './QuestionTypeParser';
import { QuestionTypeModel, TextFormatModel } from '../models/QuestionModel';
import { FormatModelToNumber } from './TextFormatTypeParser';
import { CreateChoiceRequest } from '../../main/services/protobuf/gen/testycool/choice';

export const createEmptyQuestion = (examId: number): CreateQuestionRequest => ({
  examId,
  content: 'Question here...',
  type: TypeModelToNumber(QuestionTypeModel.MC),
  format: FormatModelToNumber(TextFormatModel.PLAIN),
});

export const createEmptyChoice = (questionId: number): CreateChoiceRequest => ({
  questionId,
  content: 'Choice here...',
  format: FormatModelToNumber(TextFormatModel.PLAIN),
  isCorrect: false,
});
