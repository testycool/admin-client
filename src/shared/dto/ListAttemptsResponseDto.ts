import AttemptModel from '../models/AttemptModel';

export interface ListAttemptsResponseDto {
  attempts: AttemptModel[];
}
