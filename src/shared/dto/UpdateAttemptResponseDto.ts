import AttemptModel from '../models/AttemptModel';

export interface UpdateAttemptResponseDto {
  attempt: AttemptModel;
}
