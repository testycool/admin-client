export default interface ErrorResponseDto {
  code: number;
  details: string;
}
