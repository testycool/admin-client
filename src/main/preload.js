const { contextBridge, ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('auth', {
  ipcRenderer: {
    sendAuthRequest(address, passcode) {
      ipcRenderer.send('auth-request', { address, passcode });
    },
    once(channel, func) {
      const validChannels = ['auth-request', 'auth-error'];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
    on(channel, func) {
      const validChannels = ['auth-request', 'auth-error'];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
  },
});

contextBridge.exposeInMainWorld('exam', {
  ipcRenderer: {
    listExams(size, page) {
      ipcRenderer.send('list-exams', { size, page });
    },
    getExam(id) {
      ipcRenderer.send('get-exam', { id });
    },
    createExam(newExam) {
      ipcRenderer.send('create-exam', { newExam });
    },
    updateExam(updatedExam) {
      ipcRenderer.send('update-exam', { updatedExam });
    },
    deleteExam(id) {
      ipcRenderer.send('delete-exam', { id });
    },
    once(channel, func) {
      const validChannels = [
        'list-exams',
        'get-exam',
        'create-exam',
        'update-exam',
        'delete-exam',
        'exam-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
    on(channel, func) {
      const validChannels = [
        'list-exams',
        'get-exam',
        'create-exam',
        'update-exam',
        'delete-exam',
        'exam-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
  },
});

contextBridge.exposeInMainWorld('participant', {
  ipcRenderer: {
    listParticipants(size, page, examId) {
      ipcRenderer.send('list-participants', { size, page, examId });
    },
    createParticipant(newParticipant) {
      ipcRenderer.send('create-participant', { newParticipant });
    },
    updateParticipant(updatedParticipant) {
      ipcRenderer.send('update-participant', { updatedParticipant });
    },
    deleteParticipant(id) {
      ipcRenderer.send('delete-participant', { id });
    },
    once(channel, func) {
      const validChannels = [
        'list-participants',
        'create-participant',
        'update-participant',
        'delete-participant',
        'participant-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
    on(channel, func) {
      const validChannels = [
        'list-participants',
        'create-participant',
        'update-participant',
        'delete-participant',
        'participant-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
  },
});

contextBridge.exposeInMainWorld('question', {
  ipcRenderer: {
    listQuestions(size, page, examId) {
      ipcRenderer.send('list-questions', { size, page, examId });
    },
    getQuestion(id) {
      ipcRenderer.send('get-question', { id });
    },
    createQuestion(newQuestion) {
      ipcRenderer.send('create-question', { newQuestion });
    },
    updateQuestion(updatedQuestion) {
      ipcRenderer.send('update-question', { updatedQuestion });
    },
    updateQuestionType(updatedQuestion) {
      ipcRenderer.send('update-question-type', { updatedQuestion });
    },
    deleteQuestion(id) {
      ipcRenderer.send('delete-question', { id });
    },
    once(channel, func) {
      const validChannels = [
        'list-questions',
        'get-question',
        'create-question',
        'update-question',
        'update-question-type',
        'delete-question',
        'question-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
    on(channel, func) {
      const validChannels = [
        'list-questions',
        'get-question',
        'create-question',
        'update-question',
        'update-question-type',
        'delete-question',
        'question-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
  },
});

contextBridge.exposeInMainWorld('choice', {
  ipcRenderer: {
    listChoices(size, page, questionId) {
      ipcRenderer.send('list-choices', { size, page, filter: { questionId } });
    },
    createChoice(newChoice) {
      ipcRenderer.send('create-choice', { newChoice });
    },
    updateChoice(updatedChoice) {
      ipcRenderer.send('update-choice', { updatedChoice });
    },
    deleteChoice(id) {
      ipcRenderer.send('delete-choice', { id });
    },
    once(channel, func) {
      const validChannels = [
        'list-choices',
        'get-choice',
        'create-choice',
        'update-choice',
        'delete-choice',
        'choice-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
    on(channel, func) {
      const validChannels = [
        'list-choices',
        'get-choice',
        'create-choice',
        'update-choice',
        'delete-choice',
        'choice-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
  },
});
contextBridge.exposeInMainWorld('attempt', {
  ipcRenderer: {
    listAttempts(listAttemptRequestDto) {
      ipcRenderer.send('list-attempts', { ...listAttemptRequestDto });
    },
    getAttempt(getAttemptRequestDto) {
      ipcRenderer.send('get-attempt', { ...getAttemptRequestDto });
    },
    updateAttempt(updateAttemptRequestDto) {
      ipcRenderer.send('update-attempt', { ...updateAttemptRequestDto });
    },
    deleteAttempt(deleteAttemptRequestDto) {
      ipcRenderer.send('delete-attempt', { ...deleteAttemptRequestDto });
    },
    once(channel, func) {
      const validChannels = [
        'list-attempts',
        'get-attempt',
        'update-attempt',
        'delete-attempt',
        'attempt-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.once(channel, (event, ...args) => func(...args));
      }
    },
    on(channel, func) {
      const validChannels = [
        'list-attempts',
        'get-attempt',
        'update-attempt',
        'delete-attempt',
        'attempt-error',
      ];
      if (validChannels.includes(channel)) {
        // Deliberately strip event as it includes `sender`
        ipcRenderer.on(channel, (event, ...args) => func(...args));
      }
    },
  },
});
