import { ipcMain } from 'electron';
import { GetAttemptRequestDto } from '../../shared/dto/GetAttemptRequestDto';
import Services, { GRPCCredential } from '../services/services';
import { Metadata } from '@grpc/grpc-js';
import { AttemptServiceClient } from '../services/protobuf/gen/testycool/attempt';
import store from '../store';
import { UpdateAttemptRequestDto } from '../../shared/dto/UpdateAttemptRequestDto';
import attemptAction from '../../shared/actions/AttemptAction';
import mainStore from '../store';
import { ListAttemptsRequestDto } from '../../shared/dto/ListAttemptsRequestDto';

export const attempt = () => {
  ipcMain.on('list-attempts', (event, args: ListAttemptsRequestDto) => {
    const address = mainStore.getState().AuthReducer.address;
    if (!Services.attempt) {
      Services.attempt = new AttemptServiceClient(
        address || '',
        GRPCCredential.getCredentials()
      );
    }

    const accessToken = store.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.attempt.listAttempts(
      { filter: args.filter, page: 1, size: 0 },
      metadata,
      (error, response) => {
        if (error) {
          event.sender.send('attempt-error', {
            code: error.code,
            details: error.details,
          });
        }
        if (response) {
          event.reply('list-attempts', response);
        }
      }
    );
  });
  ipcMain.on('get-attempt', (event, args: GetAttemptRequestDto) => {
    const accessToken = store.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.attempt?.getAttempt(
      { id: args.id, participantId: args.participantId },
      metadata,
      (error, response) => {
        if (error) {
          event.sender.send('attempt-error', {
            code: error.code,
            details: error.details,
          });
        }
        if (response) {
          store.dispatch(attemptAction.getAttempt.fulfilled(response));
          event.reply('get-attempt', response);
        }
      }
    );
  });
  ipcMain.on('update-attempt', (event, args: UpdateAttemptRequestDto) => {
    const accessToken = store.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('Authorization', `Bearer ${accessToken}`);

    Services.attempt?.updateAttempt(
      {
        attempt: {
          ...args.attempt,
          createdAt: new Date(args.attempt?.createdAt || ''),
          updatedAt: new Date(args.attempt?.updatedAt || ''),
        },
      },
      metadata,
      (error, response) => {
        if (error) {
          event.sender.send('attempt-error', {
            code: error.code,
            details: error.details,
          });
        }
        if (response) {
          store.dispatch(attemptAction.updateAttempt.fulfilled(response));
          event.reply('update-attempt', response);
        }
      }
    );
  });
};

export default attempt;
