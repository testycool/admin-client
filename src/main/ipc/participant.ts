import { ipcMain } from 'electron';
import { ChannelCredentials, Metadata } from '@grpc/grpc-js';
import Services from '../services/services';
import mainStore from '../store';
import {
  CreateParticipantRequest,
  DeleteParticipantRequest,
  Participant,
  ParticipantServiceClient,
} from '../services/protobuf/gen/testycool/participant';
import ListParticipantsRequestDto from '../../shared/dto/ListParticipantsRequestDto';

export default function participant() {
  ipcMain.on('list-participants', (event, args: ListParticipantsRequestDto) => {
    const { address } = mainStore.getState().AuthReducer;
    if (!Services.participant && address) {
      Services.participant = new ParticipantServiceClient(
        address,
        ChannelCredentials.createInsecure()
      );
    }
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.participant?.listParticipants(
      {
        size: args.size,
        page: args.page,
        filter: { examId: args.examId },
      },
      metadata,
      (err, response) => {
        if (err) {
          event.sender.send('participant-error', {
            code: err.code as number,
            details: err.details as string,
          });
        }
        if (response) {
          event.reply('list-participants', response);
        }
      }
    );
  });
  ipcMain.on(
    'create-participant',
    (event, args: { newParticipant: CreateParticipantRequest }) => {
      const token = mainStore.getState().AuthReducer.accessToken;
      const metadata = new Metadata();
      metadata.set('authorization', `Bearer ${token}`);
      Services.participant?.createParticipant(
        args.newParticipant,
        metadata,
        (err, response) => {
          if (err) {
            event.sender.send('participant-error', {
              code: err.code as number,
              details: err.details as string,
            });
          }
          if (response) {
            event.reply('create-participant', response);
          }
        }
      );
    }
  );
  ipcMain.on(
    'update-participant',
    (event, args: { updatedParticipant: Participant }) => {
      const token = mainStore.getState().AuthReducer.accessToken;
      const metadata = new Metadata();
      metadata.set('authorization', `Bearer ${token}`);
      Services.participant?.updateParticipant(
        { participant: args.updatedParticipant },
        metadata,
        (err, response) => {
          if (err) {
            event.sender.send('participant-error', {
              code: err.code as number,
              details: err.details as string,
            });
          }
          if (response) {
            event.reply('update-participant', response);
          }
        }
      );
    }
  );
  ipcMain.on('delete-participant', (event, args: DeleteParticipantRequest) => {
    const token = mainStore.getState().AuthReducer.accessToken;
    const metadata = new Metadata();
    metadata.set('authorization', `Bearer ${token}`);
    Services.participant?.deleteParticipant(args, metadata, (err, response) => {
      if (err) {
        event.sender.send('participant-error', {
          code: err.code as number,
          details: err.details as string,
        });
      }
      if (response) {
        event.reply('delete-participant', { id: args.id });
      }
    });
  });
}
