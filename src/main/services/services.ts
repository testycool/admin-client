import { ChannelCredentials } from '@grpc/grpc-js';
import store from '../store';
import { AuthServiceClient } from './protobuf/gen/testycool/auth';
import { ExamServiceClient } from './protobuf/gen/testycool/exam';
import { ParticipantServiceClient } from './protobuf/gen/testycool/participant';
import { QuestionServiceClient } from './protobuf/gen/testycool/question';
import { ChoiceServiceClient } from './protobuf/gen/testycool/choice';
import { AnswerServiceClient } from './protobuf/gen/testycool/answer';
import { AttemptServiceClient } from './protobuf/gen/testycool/attempt';
import { readFileSync } from 'fs';
import * as path from 'path';
import { AnalyticsServiceClient } from './protobuf/gen/testycool/analytics';

const services: {
  auth: AuthServiceClient | undefined;
  exam: ExamServiceClient | undefined;
  participant: ParticipantServiceClient | undefined;
  question: QuestionServiceClient | undefined;
  choice: ChoiceServiceClient | undefined;
  answer: AnswerServiceClient | undefined;
  attempt: AttemptServiceClient | undefined;
  analytics: AnalyticsServiceClient | undefined;
} = {
  auth: undefined,
  exam: undefined,
  participant: undefined,
  question: undefined,
  choice: undefined,
  answer: undefined,
  attempt: undefined,
  analytics: undefined,
};

export class GRPCCredential {
  public static getCredentials() {
    if (this.credentials) {
      return this.credentials;
    }

    const certFile = path.join(
      process.resourcesPath,
      'cert',
      'testycool-root.pem'
    );
    try {
      const cert = readFileSync(certFile);

      this.credentials = ChannelCredentials.createSsl(cert);
    } catch (e) {
      console.error('unable to read certificate at', certFile);
      this.credentials = ChannelCredentials.createInsecure();
    }

    return this.credentials;
  }

  private static credentials: ChannelCredentials | null = null;
}

const Services = {
  auth: ((address: string | undefined) => {
    if (!services.auth && address) {
      services.auth = new AuthServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }
    return services.auth;
  })(store ? store.getState().AuthReducer.address : undefined),
  exam: ((address: string | undefined) => {
    if (!services.exam && address) {
      services.exam = new ExamServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }
    return services.exam;
  })(store ? store.getState().AuthReducer.address : undefined),
  participant: ((address: string | undefined) => {
    if (!services.participant && address) {
      services.participant = new ParticipantServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }
    return services.participant;
  })(store ? store.getState().AuthReducer.address : undefined),
  question: ((address: string | undefined) => {
    if (!services.question && address) {
      services.question = new QuestionServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }
    return services.question;
  })(store ? store.getState().AuthReducer.address : undefined),
  choice: ((address: string | undefined) => {
    if (!services.choice && address) {
      services.choice = new ChoiceServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }
    return services.choice;
  })(store ? store.getState().AuthReducer.address : undefined),
  answer: ((address: string | undefined) => {
    if (!services.answer && address) {
      services.answer = new AnswerServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }
    return services.answer;
  })(store ? store.getState().AuthReducer.address : undefined),
  attempt: ((address: string | undefined) => {
    if (!services.attempt && address) {
      services.attempt = new AttemptServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }
    return services.attempt;
  })(store ? store.getState().AuthReducer.address : undefined),
  analytics: ((address: string | undefined) => {
    if (!services.analytics && address) {
      services.analytics = new AnalyticsServiceClient(
        address,
        GRPCCredential.getCredentials()
      );
    }
    return services.analytics;
  })(store ? store.getState().AuthReducer.address : undefined),
};

export default Services;
