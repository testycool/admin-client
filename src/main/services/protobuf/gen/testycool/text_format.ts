/* eslint-disable */
import Long from 'long';
import _m0 from 'protobufjs/minimal';

export const protobufPackage = 'testycool.v1';

export enum TextFormat {
  TEXT_FORMAT_UNKNOWN = 0,
  TEXT_FORMAT_PLAIN = 1,
  TEXT_FORMAT_HTML = 2,
  TEXT_FORMAT_MARKDOWN = 3,
}

export function textFormatFromJSON(object: any): TextFormat {
  switch (object) {
    case 0:
    case 'TEXT_FORMAT_UNKNOWN':
      return TextFormat.TEXT_FORMAT_UNKNOWN;
    case 1:
    case 'TEXT_FORMAT_PLAIN':
      return TextFormat.TEXT_FORMAT_PLAIN;
    case 2:
    case 'TEXT_FORMAT_HTML':
      return TextFormat.TEXT_FORMAT_HTML;
    case 3:
    case 'TEXT_FORMAT_MARKDOWN':
      return TextFormat.TEXT_FORMAT_MARKDOWN;
    default:
      throw new globalThis.Error(
        'Unrecognized enum value ' + object + ' for enum TextFormat'
      );
  }
}

export function textFormatToJSON(object: TextFormat): string {
  switch (object) {
    case TextFormat.TEXT_FORMAT_UNKNOWN:
      return 'TEXT_FORMAT_UNKNOWN';
    case TextFormat.TEXT_FORMAT_PLAIN:
      return 'TEXT_FORMAT_PLAIN';
    case TextFormat.TEXT_FORMAT_HTML:
      return 'TEXT_FORMAT_HTML';
    case TextFormat.TEXT_FORMAT_MARKDOWN:
      return 'TEXT_FORMAT_MARKDOWN';
    default:
      return 'UNKNOWN';
  }
}

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
