/* eslint-disable */
import Long from 'long';
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from '@grpc/grpc-js';
import _m0 from 'protobufjs/minimal';
import { Timestamp } from '../google/protobuf/timestamp';
import { Empty } from '../google/protobuf/empty';

export const protobufPackage = 'testycool.v1';

export interface GetExamRequest {
  id: number | undefined;
  password: string | undefined;
}

export interface GetExamResponse {
  exam: Exam | undefined;
}

export interface ListExamsRequest {
  size: number;
  page: number;
  filter: ListExamsRequest_Filter | undefined;
}

export interface ListExamsRequest_Filter {}

export interface ListExamsResponse {
  exams: Exam[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateExamRequest {
  password: string;
  title: string;
  timeLimit: number;
  startAt: Date | undefined;
}

export interface CreateExamResponse {
  exam: Exam | undefined;
}

export interface UpdateExamRequest {
  exam: Exam | undefined;
}

export interface UpdateExamResponse {
  exam: Exam | undefined;
}

export interface DeleteExamRequest {
  id: number;
}

export interface Exam {
  id: number;
  password: string;
  title: string;
  timeLimit: number;
  startAt: Date | undefined;
  status: Exam_Status;
}

export enum Exam_Status {
  UNKNOWN = 0,
  WAITING = 1,
  STARTED = 2,
  DONE = 3,
}

export function exam_StatusFromJSON(object: any): Exam_Status {
  switch (object) {
    case 0:
    case 'UNKNOWN':
      return Exam_Status.UNKNOWN;
    case 1:
    case 'WAITING':
      return Exam_Status.WAITING;
    case 2:
    case 'STARTED':
      return Exam_Status.STARTED;
    case 3:
    case 'DONE':
      return Exam_Status.DONE;
    default:
      throw new globalThis.Error(
        'Unrecognized enum value ' + object + ' for enum Exam_Status'
      );
  }
}

export function exam_StatusToJSON(object: Exam_Status): string {
  switch (object) {
    case Exam_Status.UNKNOWN:
      return 'UNKNOWN';
    case Exam_Status.WAITING:
      return 'WAITING';
    case Exam_Status.STARTED:
      return 'STARTED';
    case Exam_Status.DONE:
      return 'DONE';
    default:
      return 'UNKNOWN';
  }
}

const baseGetExamRequest: object = {};

export const GetExamRequest = {
  encode(
    message: GetExamRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== undefined) {
      writer.uint32(8).int32(message.id);
    }
    if (message.password !== undefined) {
      writer.uint32(18).string(message.password);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetExamRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetExamRequest } as GetExamRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.password = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetExamRequest {
    const message = { ...baseGetExamRequest } as GetExamRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = undefined;
    }
    if (object.password !== undefined && object.password !== null) {
      message.password = String(object.password);
    } else {
      message.password = undefined;
    }
    return message;
  },

  toJSON(message: GetExamRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.password !== undefined && (obj.password = message.password);
    return obj;
  },

  fromPartial(object: DeepPartial<GetExamRequest>): GetExamRequest {
    const message = { ...baseGetExamRequest } as GetExamRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = undefined;
    }
    if (object.password !== undefined && object.password !== null) {
      message.password = object.password;
    } else {
      message.password = undefined;
    }
    return message;
  },
};

const baseGetExamResponse: object = {};

export const GetExamResponse = {
  encode(
    message: GetExamResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.exam !== undefined) {
      Exam.encode(message.exam, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetExamResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetExamResponse } as GetExamResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exam = Exam.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetExamResponse {
    const message = { ...baseGetExamResponse } as GetExamResponse;
    if (object.exam !== undefined && object.exam !== null) {
      message.exam = Exam.fromJSON(object.exam);
    } else {
      message.exam = undefined;
    }
    return message;
  },

  toJSON(message: GetExamResponse): unknown {
    const obj: any = {};
    message.exam !== undefined &&
      (obj.exam = message.exam ? Exam.toJSON(message.exam) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<GetExamResponse>): GetExamResponse {
    const message = { ...baseGetExamResponse } as GetExamResponse;
    if (object.exam !== undefined && object.exam !== null) {
      message.exam = Exam.fromPartial(object.exam);
    } else {
      message.exam = undefined;
    }
    return message;
  },
};

const baseListExamsRequest: object = { size: 0, page: 0 };

export const ListExamsRequest = {
  encode(
    message: ListExamsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListExamsRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListExamsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListExamsRequest } as ListExamsRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListExamsRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListExamsRequest {
    const message = { ...baseListExamsRequest } as ListExamsRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListExamsRequest_Filter.fromJSON(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },

  toJSON(message: ListExamsRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListExamsRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<ListExamsRequest>): ListExamsRequest {
    const message = { ...baseListExamsRequest } as ListExamsRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListExamsRequest_Filter.fromPartial(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },
};

const baseListExamsRequest_Filter: object = {};

export const ListExamsRequest_Filter = {
  encode(
    _: ListExamsRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListExamsRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseListExamsRequest_Filter,
    } as ListExamsRequest_Filter;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): ListExamsRequest_Filter {
    const message = {
      ...baseListExamsRequest_Filter,
    } as ListExamsRequest_Filter;
    return message;
  },

  toJSON(_: ListExamsRequest_Filter): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial(
    _: DeepPartial<ListExamsRequest_Filter>
  ): ListExamsRequest_Filter {
    const message = {
      ...baseListExamsRequest_Filter,
    } as ListExamsRequest_Filter;
    return message;
  },
};

const baseListExamsResponse: object = { size: 0, page: 0, totalSize: 0 };

export const ListExamsResponse = {
  encode(
    message: ListExamsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.exams) {
      Exam.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListExamsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListExamsResponse } as ListExamsResponse;
    message.exams = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exams.push(Exam.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListExamsResponse {
    const message = { ...baseListExamsResponse } as ListExamsResponse;
    message.exams = [];
    if (object.exams !== undefined && object.exams !== null) {
      for (const e of object.exams) {
        message.exams.push(Exam.fromJSON(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = Number(object.totalSize);
    } else {
      message.totalSize = 0;
    }
    return message;
  },

  toJSON(message: ListExamsResponse): unknown {
    const obj: any = {};
    if (message.exams) {
      obj.exams = message.exams.map((e) => (e ? Exam.toJSON(e) : undefined));
    } else {
      obj.exams = [];
    }
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.totalSize !== undefined && (obj.totalSize = message.totalSize);
    return obj;
  },

  fromPartial(object: DeepPartial<ListExamsResponse>): ListExamsResponse {
    const message = { ...baseListExamsResponse } as ListExamsResponse;
    message.exams = [];
    if (object.exams !== undefined && object.exams !== null) {
      for (const e of object.exams) {
        message.exams.push(Exam.fromPartial(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = object.totalSize;
    } else {
      message.totalSize = 0;
    }
    return message;
  },
};

const baseCreateExamRequest: object = { password: '', title: '', timeLimit: 0 };

export const CreateExamRequest = {
  encode(
    message: CreateExamRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.password !== '') {
      writer.uint32(10).string(message.password);
    }
    if (message.title !== '') {
      writer.uint32(18).string(message.title);
    }
    if (message.timeLimit !== 0) {
      writer.uint32(24).int32(message.timeLimit);
    }
    if (message.startAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.startAt),
        writer.uint32(34).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CreateExamRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCreateExamRequest } as CreateExamRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.password = reader.string();
          break;
        case 2:
          message.title = reader.string();
          break;
        case 3:
          message.timeLimit = reader.int32();
          break;
        case 4:
          message.startAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateExamRequest {
    const message = { ...baseCreateExamRequest } as CreateExamRequest;
    if (object.password !== undefined && object.password !== null) {
      message.password = String(object.password);
    } else {
      message.password = '';
    }
    if (object.title !== undefined && object.title !== null) {
      message.title = String(object.title);
    } else {
      message.title = '';
    }
    if (object.timeLimit !== undefined && object.timeLimit !== null) {
      message.timeLimit = Number(object.timeLimit);
    } else {
      message.timeLimit = 0;
    }
    if (object.startAt !== undefined && object.startAt !== null) {
      message.startAt = fromJsonTimestamp(object.startAt);
    } else {
      message.startAt = undefined;
    }
    return message;
  },

  toJSON(message: CreateExamRequest): unknown {
    const obj: any = {};
    message.password !== undefined && (obj.password = message.password);
    message.title !== undefined && (obj.title = message.title);
    message.timeLimit !== undefined && (obj.timeLimit = message.timeLimit);
    message.startAt !== undefined &&
      (obj.startAt = message.startAt.toISOString());
    return obj;
  },

  fromPartial(object: DeepPartial<CreateExamRequest>): CreateExamRequest {
    const message = { ...baseCreateExamRequest } as CreateExamRequest;
    if (object.password !== undefined && object.password !== null) {
      message.password = object.password;
    } else {
      message.password = '';
    }
    if (object.title !== undefined && object.title !== null) {
      message.title = object.title;
    } else {
      message.title = '';
    }
    if (object.timeLimit !== undefined && object.timeLimit !== null) {
      message.timeLimit = object.timeLimit;
    } else {
      message.timeLimit = 0;
    }
    if (object.startAt !== undefined && object.startAt !== null) {
      message.startAt = object.startAt;
    } else {
      message.startAt = undefined;
    }
    return message;
  },
};

const baseCreateExamResponse: object = {};

export const CreateExamResponse = {
  encode(
    message: CreateExamResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.exam !== undefined) {
      Exam.encode(message.exam, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): CreateExamResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCreateExamResponse } as CreateExamResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exam = Exam.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateExamResponse {
    const message = { ...baseCreateExamResponse } as CreateExamResponse;
    if (object.exam !== undefined && object.exam !== null) {
      message.exam = Exam.fromJSON(object.exam);
    } else {
      message.exam = undefined;
    }
    return message;
  },

  toJSON(message: CreateExamResponse): unknown {
    const obj: any = {};
    message.exam !== undefined &&
      (obj.exam = message.exam ? Exam.toJSON(message.exam) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<CreateExamResponse>): CreateExamResponse {
    const message = { ...baseCreateExamResponse } as CreateExamResponse;
    if (object.exam !== undefined && object.exam !== null) {
      message.exam = Exam.fromPartial(object.exam);
    } else {
      message.exam = undefined;
    }
    return message;
  },
};

const baseUpdateExamRequest: object = {};

export const UpdateExamRequest = {
  encode(
    message: UpdateExamRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.exam !== undefined) {
      Exam.encode(message.exam, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UpdateExamRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUpdateExamRequest } as UpdateExamRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exam = Exam.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateExamRequest {
    const message = { ...baseUpdateExamRequest } as UpdateExamRequest;
    if (object.exam !== undefined && object.exam !== null) {
      message.exam = Exam.fromJSON(object.exam);
    } else {
      message.exam = undefined;
    }
    return message;
  },

  toJSON(message: UpdateExamRequest): unknown {
    const obj: any = {};
    message.exam !== undefined &&
      (obj.exam = message.exam ? Exam.toJSON(message.exam) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<UpdateExamRequest>): UpdateExamRequest {
    const message = { ...baseUpdateExamRequest } as UpdateExamRequest;
    if (object.exam !== undefined && object.exam !== null) {
      message.exam = Exam.fromPartial(object.exam);
    } else {
      message.exam = undefined;
    }
    return message;
  },
};

const baseUpdateExamResponse: object = {};

export const UpdateExamResponse = {
  encode(
    message: UpdateExamResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.exam !== undefined) {
      Exam.encode(message.exam, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UpdateExamResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUpdateExamResponse } as UpdateExamResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.exam = Exam.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateExamResponse {
    const message = { ...baseUpdateExamResponse } as UpdateExamResponse;
    if (object.exam !== undefined && object.exam !== null) {
      message.exam = Exam.fromJSON(object.exam);
    } else {
      message.exam = undefined;
    }
    return message;
  },

  toJSON(message: UpdateExamResponse): unknown {
    const obj: any = {};
    message.exam !== undefined &&
      (obj.exam = message.exam ? Exam.toJSON(message.exam) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<UpdateExamResponse>): UpdateExamResponse {
    const message = { ...baseUpdateExamResponse } as UpdateExamResponse;
    if (object.exam !== undefined && object.exam !== null) {
      message.exam = Exam.fromPartial(object.exam);
    } else {
      message.exam = undefined;
    }
    return message;
  },
};

const baseDeleteExamRequest: object = { id: 0 };

export const DeleteExamRequest = {
  encode(
    message: DeleteExamRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DeleteExamRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseDeleteExamRequest } as DeleteExamRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteExamRequest {
    const message = { ...baseDeleteExamRequest } as DeleteExamRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: DeleteExamRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<DeleteExamRequest>): DeleteExamRequest {
    const message = { ...baseDeleteExamRequest } as DeleteExamRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseExam: object = {
  id: 0,
  password: '',
  title: '',
  timeLimit: 0,
  status: 0,
};

export const Exam = {
  encode(message: Exam, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.password !== '') {
      writer.uint32(18).string(message.password);
    }
    if (message.title !== '') {
      writer.uint32(26).string(message.title);
    }
    if (message.timeLimit !== 0) {
      writer.uint32(32).int32(message.timeLimit);
    }
    if (message.startAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.startAt),
        writer.uint32(42).fork()
      ).ldelim();
    }
    if (message.status !== 0) {
      writer.uint32(48).int32(message.status);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Exam {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseExam } as Exam;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.password = reader.string();
          break;
        case 3:
          message.title = reader.string();
          break;
        case 4:
          message.timeLimit = reader.int32();
          break;
        case 5:
          message.startAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        case 6:
          message.status = reader.int32() as any;
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Exam {
    const message = { ...baseExam } as Exam;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.password !== undefined && object.password !== null) {
      message.password = String(object.password);
    } else {
      message.password = '';
    }
    if (object.title !== undefined && object.title !== null) {
      message.title = String(object.title);
    } else {
      message.title = '';
    }
    if (object.timeLimit !== undefined && object.timeLimit !== null) {
      message.timeLimit = Number(object.timeLimit);
    } else {
      message.timeLimit = 0;
    }
    if (object.startAt !== undefined && object.startAt !== null) {
      message.startAt = fromJsonTimestamp(object.startAt);
    } else {
      message.startAt = undefined;
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = exam_StatusFromJSON(object.status);
    } else {
      message.status = 0;
    }
    return message;
  },

  toJSON(message: Exam): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.password !== undefined && (obj.password = message.password);
    message.title !== undefined && (obj.title = message.title);
    message.timeLimit !== undefined && (obj.timeLimit = message.timeLimit);
    message.startAt !== undefined &&
      (obj.startAt = message.startAt.toISOString());
    message.status !== undefined &&
      (obj.status = exam_StatusToJSON(message.status));
    return obj;
  },

  fromPartial(object: DeepPartial<Exam>): Exam {
    const message = { ...baseExam } as Exam;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (object.password !== undefined && object.password !== null) {
      message.password = object.password;
    } else {
      message.password = '';
    }
    if (object.title !== undefined && object.title !== null) {
      message.title = object.title;
    } else {
      message.title = '';
    }
    if (object.timeLimit !== undefined && object.timeLimit !== null) {
      message.timeLimit = object.timeLimit;
    } else {
      message.timeLimit = 0;
    }
    if (object.startAt !== undefined && object.startAt !== null) {
      message.startAt = object.startAt;
    } else {
      message.startAt = undefined;
    }
    if (object.status !== undefined && object.status !== null) {
      message.status = object.status;
    } else {
      message.status = 0;
    }
    return message;
  },
};

export const ExamServiceService = {
  getExam: {
    path: '/testycool.v1.ExamService/GetExam',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetExamRequest) =>
      Buffer.from(GetExamRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetExamRequest.decode(value),
    responseSerialize: (value: GetExamResponse) =>
      Buffer.from(GetExamResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetExamResponse.decode(value),
  },
  listExams: {
    path: '/testycool.v1.ExamService/ListExams',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListExamsRequest) =>
      Buffer.from(ListExamsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => ListExamsRequest.decode(value),
    responseSerialize: (value: ListExamsResponse) =>
      Buffer.from(ListExamsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => ListExamsResponse.decode(value),
  },
  createExam: {
    path: '/testycool.v1.ExamService/CreateExam',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateExamRequest) =>
      Buffer.from(CreateExamRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => CreateExamRequest.decode(value),
    responseSerialize: (value: CreateExamResponse) =>
      Buffer.from(CreateExamResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => CreateExamResponse.decode(value),
  },
  updateExam: {
    path: '/testycool.v1.ExamService/UpdateExam',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateExamRequest) =>
      Buffer.from(UpdateExamRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => UpdateExamRequest.decode(value),
    responseSerialize: (value: UpdateExamResponse) =>
      Buffer.from(UpdateExamResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => UpdateExamResponse.decode(value),
  },
  deleteExam: {
    path: '/testycool.v1.ExamService/DeleteExam',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteExamRequest) =>
      Buffer.from(DeleteExamRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => DeleteExamRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface ExamServiceServer extends UntypedServiceImplementation {
  getExam: handleUnaryCall<GetExamRequest, GetExamResponse>;
  listExams: handleUnaryCall<ListExamsRequest, ListExamsResponse>;
  createExam: handleUnaryCall<CreateExamRequest, CreateExamResponse>;
  updateExam: handleUnaryCall<UpdateExamRequest, UpdateExamResponse>;
  deleteExam: handleUnaryCall<DeleteExamRequest, Empty>;
}

export interface ExamServiceClient extends Client {
  getExam(
    request: GetExamRequest,
    callback: (error: ServiceError | null, response: GetExamResponse) => void
  ): ClientUnaryCall;
  getExam(
    request: GetExamRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: GetExamResponse) => void
  ): ClientUnaryCall;
  getExam(
    request: GetExamRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: GetExamResponse) => void
  ): ClientUnaryCall;
  listExams(
    request: ListExamsRequest,
    callback: (error: ServiceError | null, response: ListExamsResponse) => void
  ): ClientUnaryCall;
  listExams(
    request: ListExamsRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: ListExamsResponse) => void
  ): ClientUnaryCall;
  listExams(
    request: ListExamsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: ListExamsResponse) => void
  ): ClientUnaryCall;
  createExam(
    request: CreateExamRequest,
    callback: (error: ServiceError | null, response: CreateExamResponse) => void
  ): ClientUnaryCall;
  createExam(
    request: CreateExamRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: CreateExamResponse) => void
  ): ClientUnaryCall;
  createExam(
    request: CreateExamRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: CreateExamResponse) => void
  ): ClientUnaryCall;
  updateExam(
    request: UpdateExamRequest,
    callback: (error: ServiceError | null, response: UpdateExamResponse) => void
  ): ClientUnaryCall;
  updateExam(
    request: UpdateExamRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: UpdateExamResponse) => void
  ): ClientUnaryCall;
  updateExam(
    request: UpdateExamRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: UpdateExamResponse) => void
  ): ClientUnaryCall;
  deleteExam(
    request: DeleteExamRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteExam(
    request: DeleteExamRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteExam(
    request: DeleteExamRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const ExamServiceClient = makeGenericClientConstructor(
  ExamServiceService,
  'testycool.v1.ExamService'
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): ExamServiceClient;
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== 'undefined') return globalThis;
  if (typeof self !== 'undefined') return self;
  if (typeof window !== 'undefined') return window;
  if (typeof global !== 'undefined') return global;
  throw 'Unable to locate global object';
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function toTimestamp(date: Date): Timestamp {
  const seconds = date.getTime() / 1_000;
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

function fromTimestamp(t: Timestamp): Date {
  let millis = t.seconds * 1_000;
  millis += t.nanos / 1_000_000;
  return new Date(millis);
}

function fromJsonTimestamp(o: any): Date {
  if (o instanceof Date) {
    return o;
  } else if (typeof o === 'string') {
    return new Date(o);
  } else {
    return fromTimestamp(Timestamp.fromJSON(o));
  }
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
