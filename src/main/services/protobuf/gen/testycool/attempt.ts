/* eslint-disable */
import Long from 'long';
import {
  makeGenericClientConstructor,
  ChannelCredentials,
  ChannelOptions,
  UntypedServiceImplementation,
  handleUnaryCall,
  Client,
  ClientUnaryCall,
  Metadata,
  CallOptions,
  ServiceError,
} from '@grpc/grpc-js';
import _m0 from 'protobufjs/minimal';
import { Timestamp } from '../google/protobuf/timestamp';
import { Empty } from '../google/protobuf/empty';

export const protobufPackage = 'testycool.v1';

export interface GetAttemptRequest {
  id: number | undefined;
  participantId: number | undefined;
}

export interface GetAttemptResponse {
  attempt: Attempt | undefined;
}

export interface ListAttemptsRequest {
  size: number;
  page: number;
  filter: ListAttemptsRequest_Filter | undefined;
}

export interface ListAttemptsRequest_Filter {
  examId?: number | undefined;
}

export interface ListAttemptsResponse {
  attempts: Attempt[];
  size: number;
  page: number;
  totalSize: number;
}

export interface CreateAttemptRequest {
  participantId: number;
  examId: number;
  createdAt: Date | undefined;
}

export interface CreateAttemptResponse {
  attempt: Attempt | undefined;
}

export interface UpdateAttemptRequest {
  attempt: Attempt | undefined;
}

export interface UpdateAttemptResponse {
  attempt: Attempt | undefined;
}

export interface DeleteAttemptRequest {
  id: number;
}

export interface Attempt {
  id: number;
  participantId: number;
  examId: number;
  finished: boolean;
  corrects: number;
  wrongs: number;
  unanswered: number;
  createdAt: Date | undefined;
  updatedAt: Date | undefined;
}

const baseGetAttemptRequest: object = {};

export const GetAttemptRequest = {
  encode(
    message: GetAttemptRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== undefined) {
      writer.uint32(8).int32(message.id);
    }
    if (message.participantId !== undefined) {
      writer.uint32(16).int32(message.participantId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetAttemptRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetAttemptRequest } as GetAttemptRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.participantId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAttemptRequest {
    const message = { ...baseGetAttemptRequest } as GetAttemptRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = undefined;
    }
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = Number(object.participantId);
    } else {
      message.participantId = undefined;
    }
    return message;
  },

  toJSON(message: GetAttemptRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.participantId !== undefined &&
      (obj.participantId = message.participantId);
    return obj;
  },

  fromPartial(object: DeepPartial<GetAttemptRequest>): GetAttemptRequest {
    const message = { ...baseGetAttemptRequest } as GetAttemptRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = undefined;
    }
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = object.participantId;
    } else {
      message.participantId = undefined;
    }
    return message;
  },
};

const baseGetAttemptResponse: object = {};

export const GetAttemptResponse = {
  encode(
    message: GetAttemptResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.attempt !== undefined) {
      Attempt.encode(message.attempt, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GetAttemptResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseGetAttemptResponse } as GetAttemptResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.attempt = Attempt.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GetAttemptResponse {
    const message = { ...baseGetAttemptResponse } as GetAttemptResponse;
    if (object.attempt !== undefined && object.attempt !== null) {
      message.attempt = Attempt.fromJSON(object.attempt);
    } else {
      message.attempt = undefined;
    }
    return message;
  },

  toJSON(message: GetAttemptResponse): unknown {
    const obj: any = {};
    message.attempt !== undefined &&
      (obj.attempt = message.attempt
        ? Attempt.toJSON(message.attempt)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<GetAttemptResponse>): GetAttemptResponse {
    const message = { ...baseGetAttemptResponse } as GetAttemptResponse;
    if (object.attempt !== undefined && object.attempt !== null) {
      message.attempt = Attempt.fromPartial(object.attempt);
    } else {
      message.attempt = undefined;
    }
    return message;
  },
};

const baseListAttemptsRequest: object = { size: 0, page: 0 };

export const ListAttemptsRequest = {
  encode(
    message: ListAttemptsRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.size !== 0) {
      writer.uint32(8).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(16).int32(message.page);
    }
    if (message.filter !== undefined) {
      ListAttemptsRequest_Filter.encode(
        message.filter,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListAttemptsRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListAttemptsRequest } as ListAttemptsRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.size = reader.int32();
          break;
        case 2:
          message.page = reader.int32();
          break;
        case 3:
          message.filter = ListAttemptsRequest_Filter.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAttemptsRequest {
    const message = { ...baseListAttemptsRequest } as ListAttemptsRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListAttemptsRequest_Filter.fromJSON(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },

  toJSON(message: ListAttemptsRequest): unknown {
    const obj: any = {};
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.filter !== undefined &&
      (obj.filter = message.filter
        ? ListAttemptsRequest_Filter.toJSON(message.filter)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<ListAttemptsRequest>): ListAttemptsRequest {
    const message = { ...baseListAttemptsRequest } as ListAttemptsRequest;
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.filter !== undefined && object.filter !== null) {
      message.filter = ListAttemptsRequest_Filter.fromPartial(object.filter);
    } else {
      message.filter = undefined;
    }
    return message;
  },
};

const baseListAttemptsRequest_Filter: object = {};

export const ListAttemptsRequest_Filter = {
  encode(
    message: ListAttemptsRequest_Filter,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.examId !== undefined) {
      writer.uint32(16).int32(message.examId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAttemptsRequest_Filter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseListAttemptsRequest_Filter,
    } as ListAttemptsRequest_Filter;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 2:
          message.examId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAttemptsRequest_Filter {
    const message = {
      ...baseListAttemptsRequest_Filter,
    } as ListAttemptsRequest_Filter;
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = undefined;
    }
    return message;
  },

  toJSON(message: ListAttemptsRequest_Filter): unknown {
    const obj: any = {};
    message.examId !== undefined && (obj.examId = message.examId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ListAttemptsRequest_Filter>
  ): ListAttemptsRequest_Filter {
    const message = {
      ...baseListAttemptsRequest_Filter,
    } as ListAttemptsRequest_Filter;
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = object.examId;
    } else {
      message.examId = undefined;
    }
    return message;
  },
};

const baseListAttemptsResponse: object = { size: 0, page: 0, totalSize: 0 };

export const ListAttemptsResponse = {
  encode(
    message: ListAttemptsResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.attempts) {
      Attempt.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.size !== 0) {
      writer.uint32(16).int32(message.size);
    }
    if (message.page !== 0) {
      writer.uint32(24).int32(message.page);
    }
    if (message.totalSize !== 0) {
      writer.uint32(32).int32(message.totalSize);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ListAttemptsResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListAttemptsResponse } as ListAttemptsResponse;
    message.attempts = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.attempts.push(Attempt.decode(reader, reader.uint32()));
          break;
        case 2:
          message.size = reader.int32();
          break;
        case 3:
          message.page = reader.int32();
          break;
        case 4:
          message.totalSize = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListAttemptsResponse {
    const message = { ...baseListAttemptsResponse } as ListAttemptsResponse;
    message.attempts = [];
    if (object.attempts !== undefined && object.attempts !== null) {
      for (const e of object.attempts) {
        message.attempts.push(Attempt.fromJSON(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = Number(object.size);
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = Number(object.page);
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = Number(object.totalSize);
    } else {
      message.totalSize = 0;
    }
    return message;
  },

  toJSON(message: ListAttemptsResponse): unknown {
    const obj: any = {};
    if (message.attempts) {
      obj.attempts = message.attempts.map((e) =>
        e ? Attempt.toJSON(e) : undefined
      );
    } else {
      obj.attempts = [];
    }
    message.size !== undefined && (obj.size = message.size);
    message.page !== undefined && (obj.page = message.page);
    message.totalSize !== undefined && (obj.totalSize = message.totalSize);
    return obj;
  },

  fromPartial(object: DeepPartial<ListAttemptsResponse>): ListAttemptsResponse {
    const message = { ...baseListAttemptsResponse } as ListAttemptsResponse;
    message.attempts = [];
    if (object.attempts !== undefined && object.attempts !== null) {
      for (const e of object.attempts) {
        message.attempts.push(Attempt.fromPartial(e));
      }
    }
    if (object.size !== undefined && object.size !== null) {
      message.size = object.size;
    } else {
      message.size = 0;
    }
    if (object.page !== undefined && object.page !== null) {
      message.page = object.page;
    } else {
      message.page = 0;
    }
    if (object.totalSize !== undefined && object.totalSize !== null) {
      message.totalSize = object.totalSize;
    } else {
      message.totalSize = 0;
    }
    return message;
  },
};

const baseCreateAttemptRequest: object = { participantId: 0, examId: 0 };

export const CreateAttemptRequest = {
  encode(
    message: CreateAttemptRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.participantId !== 0) {
      writer.uint32(8).int32(message.participantId);
    }
    if (message.examId !== 0) {
      writer.uint32(16).int32(message.examId);
    }
    if (message.createdAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.createdAt),
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateAttemptRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCreateAttemptRequest } as CreateAttemptRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.participantId = reader.int32();
          break;
        case 2:
          message.examId = reader.int32();
          break;
        case 3:
          message.createdAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAttemptRequest {
    const message = { ...baseCreateAttemptRequest } as CreateAttemptRequest;
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = Number(object.participantId);
    } else {
      message.participantId = 0;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = 0;
    }
    if (object.createdAt !== undefined && object.createdAt !== null) {
      message.createdAt = fromJsonTimestamp(object.createdAt);
    } else {
      message.createdAt = undefined;
    }
    return message;
  },

  toJSON(message: CreateAttemptRequest): unknown {
    const obj: any = {};
    message.participantId !== undefined &&
      (obj.participantId = message.participantId);
    message.examId !== undefined && (obj.examId = message.examId);
    message.createdAt !== undefined &&
      (obj.createdAt = message.createdAt.toISOString());
    return obj;
  },

  fromPartial(object: DeepPartial<CreateAttemptRequest>): CreateAttemptRequest {
    const message = { ...baseCreateAttemptRequest } as CreateAttemptRequest;
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = object.participantId;
    } else {
      message.participantId = 0;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = object.examId;
    } else {
      message.examId = 0;
    }
    if (object.createdAt !== undefined && object.createdAt !== null) {
      message.createdAt = object.createdAt;
    } else {
      message.createdAt = undefined;
    }
    return message;
  },
};

const baseCreateAttemptResponse: object = {};

export const CreateAttemptResponse = {
  encode(
    message: CreateAttemptResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.attempt !== undefined) {
      Attempt.encode(message.attempt, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): CreateAttemptResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseCreateAttemptResponse } as CreateAttemptResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.attempt = Attempt.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): CreateAttemptResponse {
    const message = { ...baseCreateAttemptResponse } as CreateAttemptResponse;
    if (object.attempt !== undefined && object.attempt !== null) {
      message.attempt = Attempt.fromJSON(object.attempt);
    } else {
      message.attempt = undefined;
    }
    return message;
  },

  toJSON(message: CreateAttemptResponse): unknown {
    const obj: any = {};
    message.attempt !== undefined &&
      (obj.attempt = message.attempt
        ? Attempt.toJSON(message.attempt)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<CreateAttemptResponse>
  ): CreateAttemptResponse {
    const message = { ...baseCreateAttemptResponse } as CreateAttemptResponse;
    if (object.attempt !== undefined && object.attempt !== null) {
      message.attempt = Attempt.fromPartial(object.attempt);
    } else {
      message.attempt = undefined;
    }
    return message;
  },
};

const baseUpdateAttemptRequest: object = {};

export const UpdateAttemptRequest = {
  encode(
    message: UpdateAttemptRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.attempt !== undefined) {
      Attempt.encode(message.attempt, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateAttemptRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUpdateAttemptRequest } as UpdateAttemptRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.attempt = Attempt.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAttemptRequest {
    const message = { ...baseUpdateAttemptRequest } as UpdateAttemptRequest;
    if (object.attempt !== undefined && object.attempt !== null) {
      message.attempt = Attempt.fromJSON(object.attempt);
    } else {
      message.attempt = undefined;
    }
    return message;
  },

  toJSON(message: UpdateAttemptRequest): unknown {
    const obj: any = {};
    message.attempt !== undefined &&
      (obj.attempt = message.attempt
        ? Attempt.toJSON(message.attempt)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<UpdateAttemptRequest>): UpdateAttemptRequest {
    const message = { ...baseUpdateAttemptRequest } as UpdateAttemptRequest;
    if (object.attempt !== undefined && object.attempt !== null) {
      message.attempt = Attempt.fromPartial(object.attempt);
    } else {
      message.attempt = undefined;
    }
    return message;
  },
};

const baseUpdateAttemptResponse: object = {};

export const UpdateAttemptResponse = {
  encode(
    message: UpdateAttemptResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.attempt !== undefined) {
      Attempt.encode(message.attempt, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): UpdateAttemptResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseUpdateAttemptResponse } as UpdateAttemptResponse;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.attempt = Attempt.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UpdateAttemptResponse {
    const message = { ...baseUpdateAttemptResponse } as UpdateAttemptResponse;
    if (object.attempt !== undefined && object.attempt !== null) {
      message.attempt = Attempt.fromJSON(object.attempt);
    } else {
      message.attempt = undefined;
    }
    return message;
  },

  toJSON(message: UpdateAttemptResponse): unknown {
    const obj: any = {};
    message.attempt !== undefined &&
      (obj.attempt = message.attempt
        ? Attempt.toJSON(message.attempt)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<UpdateAttemptResponse>
  ): UpdateAttemptResponse {
    const message = { ...baseUpdateAttemptResponse } as UpdateAttemptResponse;
    if (object.attempt !== undefined && object.attempt !== null) {
      message.attempt = Attempt.fromPartial(object.attempt);
    } else {
      message.attempt = undefined;
    }
    return message;
  },
};

const baseDeleteAttemptRequest: object = { id: 0 };

export const DeleteAttemptRequest = {
  encode(
    message: DeleteAttemptRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): DeleteAttemptRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseDeleteAttemptRequest } as DeleteAttemptRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): DeleteAttemptRequest {
    const message = { ...baseDeleteAttemptRequest } as DeleteAttemptRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    return message;
  },

  toJSON(message: DeleteAttemptRequest): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial(object: DeepPartial<DeleteAttemptRequest>): DeleteAttemptRequest {
    const message = { ...baseDeleteAttemptRequest } as DeleteAttemptRequest;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    return message;
  },
};

const baseAttempt: object = {
  id: 0,
  participantId: 0,
  examId: 0,
  finished: false,
  corrects: 0,
  wrongs: 0,
  unanswered: 0,
};

export const Attempt = {
  encode(
    message: Attempt,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.participantId !== 0) {
      writer.uint32(16).int32(message.participantId);
    }
    if (message.examId !== 0) {
      writer.uint32(24).int32(message.examId);
    }
    if (message.finished === true) {
      writer.uint32(32).bool(message.finished);
    }
    if (message.corrects !== 0) {
      writer.uint32(40).int32(message.corrects);
    }
    if (message.wrongs !== 0) {
      writer.uint32(48).int32(message.wrongs);
    }
    if (message.unanswered !== 0) {
      writer.uint32(56).int32(message.unanswered);
    }
    if (message.createdAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.createdAt),
        writer.uint32(66).fork()
      ).ldelim();
    }
    if (message.updatedAt !== undefined) {
      Timestamp.encode(
        toTimestamp(message.updatedAt),
        writer.uint32(74).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Attempt {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAttempt } as Attempt;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.participantId = reader.int32();
          break;
        case 3:
          message.examId = reader.int32();
          break;
        case 4:
          message.finished = reader.bool();
          break;
        case 5:
          message.corrects = reader.int32();
          break;
        case 6:
          message.wrongs = reader.int32();
          break;
        case 7:
          message.unanswered = reader.int32();
          break;
        case 8:
          message.createdAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        case 9:
          message.updatedAt = fromTimestamp(
            Timestamp.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Attempt {
    const message = { ...baseAttempt } as Attempt;
    if (object.id !== undefined && object.id !== null) {
      message.id = Number(object.id);
    } else {
      message.id = 0;
    }
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = Number(object.participantId);
    } else {
      message.participantId = 0;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = Number(object.examId);
    } else {
      message.examId = 0;
    }
    if (object.finished !== undefined && object.finished !== null) {
      message.finished = Boolean(object.finished);
    } else {
      message.finished = false;
    }
    if (object.corrects !== undefined && object.corrects !== null) {
      message.corrects = Number(object.corrects);
    } else {
      message.corrects = 0;
    }
    if (object.wrongs !== undefined && object.wrongs !== null) {
      message.wrongs = Number(object.wrongs);
    } else {
      message.wrongs = 0;
    }
    if (object.unanswered !== undefined && object.unanswered !== null) {
      message.unanswered = Number(object.unanswered);
    } else {
      message.unanswered = 0;
    }
    if (object.createdAt !== undefined && object.createdAt !== null) {
      message.createdAt = fromJsonTimestamp(object.createdAt);
    } else {
      message.createdAt = undefined;
    }
    if (object.updatedAt !== undefined && object.updatedAt !== null) {
      message.updatedAt = fromJsonTimestamp(object.updatedAt);
    } else {
      message.updatedAt = undefined;
    }
    return message;
  },

  toJSON(message: Attempt): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.participantId !== undefined &&
      (obj.participantId = message.participantId);
    message.examId !== undefined && (obj.examId = message.examId);
    message.finished !== undefined && (obj.finished = message.finished);
    message.corrects !== undefined && (obj.corrects = message.corrects);
    message.wrongs !== undefined && (obj.wrongs = message.wrongs);
    message.unanswered !== undefined && (obj.unanswered = message.unanswered);
    message.createdAt !== undefined &&
      (obj.createdAt = message.createdAt.toISOString());
    message.updatedAt !== undefined &&
      (obj.updatedAt = message.updatedAt.toISOString());
    return obj;
  },

  fromPartial(object: DeepPartial<Attempt>): Attempt {
    const message = { ...baseAttempt } as Attempt;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    } else {
      message.id = 0;
    }
    if (object.participantId !== undefined && object.participantId !== null) {
      message.participantId = object.participantId;
    } else {
      message.participantId = 0;
    }
    if (object.examId !== undefined && object.examId !== null) {
      message.examId = object.examId;
    } else {
      message.examId = 0;
    }
    if (object.finished !== undefined && object.finished !== null) {
      message.finished = object.finished;
    } else {
      message.finished = false;
    }
    if (object.corrects !== undefined && object.corrects !== null) {
      message.corrects = object.corrects;
    } else {
      message.corrects = 0;
    }
    if (object.wrongs !== undefined && object.wrongs !== null) {
      message.wrongs = object.wrongs;
    } else {
      message.wrongs = 0;
    }
    if (object.unanswered !== undefined && object.unanswered !== null) {
      message.unanswered = object.unanswered;
    } else {
      message.unanswered = 0;
    }
    if (object.createdAt !== undefined && object.createdAt !== null) {
      message.createdAt = object.createdAt;
    } else {
      message.createdAt = undefined;
    }
    if (object.updatedAt !== undefined && object.updatedAt !== null) {
      message.updatedAt = object.updatedAt;
    } else {
      message.updatedAt = undefined;
    }
    return message;
  },
};

export const AttemptServiceService = {
  getAttempt: {
    path: '/testycool.v1.AttemptService/GetAttempt',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: GetAttemptRequest) =>
      Buffer.from(GetAttemptRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => GetAttemptRequest.decode(value),
    responseSerialize: (value: GetAttemptResponse) =>
      Buffer.from(GetAttemptResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => GetAttemptResponse.decode(value),
  },
  listAttempts: {
    path: '/testycool.v1.AttemptService/ListAttempts',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: ListAttemptsRequest) =>
      Buffer.from(ListAttemptsRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => ListAttemptsRequest.decode(value),
    responseSerialize: (value: ListAttemptsResponse) =>
      Buffer.from(ListAttemptsResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => ListAttemptsResponse.decode(value),
  },
  createAttempt: {
    path: '/testycool.v1.AttemptService/CreateAttempt',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: CreateAttemptRequest) =>
      Buffer.from(CreateAttemptRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => CreateAttemptRequest.decode(value),
    responseSerialize: (value: CreateAttemptResponse) =>
      Buffer.from(CreateAttemptResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => CreateAttemptResponse.decode(value),
  },
  updateAttempt: {
    path: '/testycool.v1.AttemptService/UpdateAttempt',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: UpdateAttemptRequest) =>
      Buffer.from(UpdateAttemptRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => UpdateAttemptRequest.decode(value),
    responseSerialize: (value: UpdateAttemptResponse) =>
      Buffer.from(UpdateAttemptResponse.encode(value).finish()),
    responseDeserialize: (value: Buffer) => UpdateAttemptResponse.decode(value),
  },
  deleteAttempt: {
    path: '/testycool.v1.AttemptService/DeleteAttempt',
    requestStream: false,
    responseStream: false,
    requestSerialize: (value: DeleteAttemptRequest) =>
      Buffer.from(DeleteAttemptRequest.encode(value).finish()),
    requestDeserialize: (value: Buffer) => DeleteAttemptRequest.decode(value),
    responseSerialize: (value: Empty) =>
      Buffer.from(Empty.encode(value).finish()),
    responseDeserialize: (value: Buffer) => Empty.decode(value),
  },
} as const;

export interface AttemptServiceServer extends UntypedServiceImplementation {
  getAttempt: handleUnaryCall<GetAttemptRequest, GetAttemptResponse>;
  listAttempts: handleUnaryCall<ListAttemptsRequest, ListAttemptsResponse>;
  createAttempt: handleUnaryCall<CreateAttemptRequest, CreateAttemptResponse>;
  updateAttempt: handleUnaryCall<UpdateAttemptRequest, UpdateAttemptResponse>;
  deleteAttempt: handleUnaryCall<DeleteAttemptRequest, Empty>;
}

export interface AttemptServiceClient extends Client {
  getAttempt(
    request: GetAttemptRequest,
    callback: (error: ServiceError | null, response: GetAttemptResponse) => void
  ): ClientUnaryCall;
  getAttempt(
    request: GetAttemptRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: GetAttemptResponse) => void
  ): ClientUnaryCall;
  getAttempt(
    request: GetAttemptRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: GetAttemptResponse) => void
  ): ClientUnaryCall;
  listAttempts(
    request: ListAttemptsRequest,
    callback: (
      error: ServiceError | null,
      response: ListAttemptsResponse
    ) => void
  ): ClientUnaryCall;
  listAttempts(
    request: ListAttemptsRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: ListAttemptsResponse
    ) => void
  ): ClientUnaryCall;
  listAttempts(
    request: ListAttemptsRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: ListAttemptsResponse
    ) => void
  ): ClientUnaryCall;
  createAttempt(
    request: CreateAttemptRequest,
    callback: (
      error: ServiceError | null,
      response: CreateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  createAttempt(
    request: CreateAttemptRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: CreateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  createAttempt(
    request: CreateAttemptRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: CreateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  updateAttempt(
    request: UpdateAttemptRequest,
    callback: (
      error: ServiceError | null,
      response: UpdateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  updateAttempt(
    request: UpdateAttemptRequest,
    metadata: Metadata,
    callback: (
      error: ServiceError | null,
      response: UpdateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  updateAttempt(
    request: UpdateAttemptRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (
      error: ServiceError | null,
      response: UpdateAttemptResponse
    ) => void
  ): ClientUnaryCall;
  deleteAttempt(
    request: DeleteAttemptRequest,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAttempt(
    request: DeleteAttemptRequest,
    metadata: Metadata,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
  deleteAttempt(
    request: DeleteAttemptRequest,
    metadata: Metadata,
    options: Partial<CallOptions>,
    callback: (error: ServiceError | null, response: Empty) => void
  ): ClientUnaryCall;
}

export const AttemptServiceClient = makeGenericClientConstructor(
  AttemptServiceService,
  'testycool.v1.AttemptService'
) as unknown as {
  new (
    address: string,
    credentials: ChannelCredentials,
    options?: Partial<ChannelOptions>
  ): AttemptServiceClient;
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

function toTimestamp(date: Date): Timestamp {
  const seconds = date.getTime() / 1_000;
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

function fromTimestamp(t: Timestamp): Date {
  let millis = t.seconds * 1_000;
  millis += t.nanos / 1_000_000;
  return new Date(millis);
}

function fromJsonTimestamp(o: any): Date {
  if (o instanceof Date) {
    return o;
  } else if (typeof o === 'string') {
    return new Date(o);
  } else {
    return fromTimestamp(Timestamp.fromJSON(o));
  }
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
